package fr.julien.quizz.junit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class Question8 {
    private static class Personne {
	private String nom;

	public Personne(String nom) {
	    this.nom = nom;
	}

	@Override
	public boolean equals(Object obj) {
	    if (this == obj) {
		return true;
	    }
	    if (obj == null) {
		return false;
	    }
	    if (getClass() != obj.getClass()) {
		return false;
	    }
	    Personne other = (Personne) obj;
	    if (nom == null) {
		if (other.nom != null) {
		    return false;
		}
	    }
	    else if (!nom.equals(other.nom)) {
		return false;
	    }
	    return true;
	}
    }

    @Test
    public void tester() {
	Personne p1 = new Personne("Julien");
	Assertions.assertEquals(new Personne("Julien"), p1);
    }
}
