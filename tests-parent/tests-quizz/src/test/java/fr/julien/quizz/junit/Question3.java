package fr.julien.quizz.junit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Test;

public class Question3 {
    @Test
    public void tester() {
	String s = "texte";
	Assumptions.assumeTrue(s.equals("autre"));
	Assertions.assertEquals("autre", s);
    }
}
