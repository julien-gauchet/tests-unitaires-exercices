package fr.julien.quizz.junit;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class Question9 {
    @Test
    public void tester() {
	List<String> l1 = new ArrayList<>();
	l1.add("A");
	l1.add("B");
	List<String> l2 = new ArrayList<>();
	l2.add("A");
	l2.add("B");
	Assertions.assertEquals(l1, l2);
    }
}
