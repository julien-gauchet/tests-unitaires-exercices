package fr.julien.quizz.junit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class Question7 {
    private static class Personne {
	private String nom;

	public Personne(String nom) {
	    this.nom = nom;
	}
    }

    @Test
    public void tester() {
	Personne p1 = new Personne("Julien");
	Assertions.assertEquals(new Personne("Julien"), p1);
    }
}
