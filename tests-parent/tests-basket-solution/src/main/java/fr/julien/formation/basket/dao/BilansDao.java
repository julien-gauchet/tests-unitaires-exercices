package fr.julien.formation.basket.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.julien.formation.basket.exception.BasketException;
import fr.julien.formation.basket.model.BilanJoueur;
import fr.julien.formation.basket.model.Equipe;
import fr.julien.formation.basket.model.Joueur;

/**
 * Classe permettant d'accéder à la table bilans
 * @author Julien Gauchet
 */
public class BilansDao {

    private static BilansDao instance = new BilansDao();
    private static final String REQUETE_SELECT_BY_ID = "SELECT j.nom AS nom_joueur, j.prenom AS prenom_joueur, e.nom AS nom_equipe, b.* FROM bilans b INNER JOIN joueurs j ON b.id_joueur=j.id INNER JOIN equipes e ON e.id=j.id_equipe WHERE b.id_match=?";
    private static final String REQUETE_INSERTION = "INSERT INTO bilans (id_match, id_joueur, lancer_francs_reussis, lancer_francs, deux_points_reussis, deux_points, trois_points_reussis, trois_points, rebonds, fautes, passes_decisives,ballons_perdus, interceptions, contres) VALUES (?, (SELECT id FROM joueurs WHERE nom=? AND prenom=?), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    private BilansDao() {
        super();
    }

    /**
     * Méthode permettant de récupérer la liste des bilans des joueurs pour un match donné
     * @param cnx
     *            La connexion à la base de données
     * @param id
     *            L'identifiant du match recherché
     * @return la liste des {@link BilanJoueur}
     * @throws BasketException
     */
    public List<BilanJoueur> getBilansByMatch(Connection cnx, int id) throws BasketException {
        List<BilanJoueur> res = new ArrayList<>();
        try (PreparedStatement stmt = cnx.prepareStatement(REQUETE_SELECT_BY_ID)) {
            stmt.setInt(1, id);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    res.add(build(rs));
                }
            }
        }
        catch (SQLException e) {
            throw new BasketException(e.getMessage(), e);
        }
        return res;
    }

    /**
     * Méthode permettant d'insérer le bilan d'un joueur
     * @param cnx
     *            La connexion à la base de données
     * @param b
     *            Le bilan à insérer
     * @param idMatch
     *            L'identifiant du match
     * @throws BasketException
     */
    public void inserer(Connection cnx, BilanJoueur b, int idMatch) throws BasketException {
        try (PreparedStatement stmt = cnx.prepareStatement(REQUETE_INSERTION)) {
            int cpt = 1;
            stmt.setInt(cpt++, idMatch);
            stmt.setString(cpt++, b.getJoueur().getNom());
            stmt.setString(cpt++, b.getJoueur().getPrenom());
            stmt.setInt(cpt++, b.getLancerFrancsReussis());
            stmt.setInt(cpt++, b.getLancerFrancs());
            stmt.setInt(cpt++, b.getDeuxPointsReussis());
            stmt.setInt(cpt++, b.getDeuxPoints());
            stmt.setInt(cpt++, b.getTroisPointsReussis());
            stmt.setInt(cpt++, b.getTroisPoints());
            stmt.setInt(cpt++, b.getRebonds());
            stmt.setInt(cpt++, b.getFautes());
            stmt.setInt(cpt++, b.getPassesDecisives());
            stmt.setInt(cpt++, b.getBallonsPerdus());
            stmt.setInt(cpt++, b.getInterceptions());
            stmt.setInt(cpt, b.getContres());
            stmt.executeUpdate();
        }
        catch (SQLException e) {
            throw new BasketException(e.getMessage(), e);
        }
    }

    private BilanJoueur build(ResultSet rs) throws SQLException {
        Equipe e = new Equipe(rs.getString("nom_equipe"));
        Joueur j = new Joueur(rs.getString("nom_joueur"), rs.getString("prenom_joueur"), e);
        int lancerFrancs = rs.getInt("lancer_francs");
        int lancerFrancsReussis = rs.getInt("lancer_francs_reussis");
        int deuxPoints = rs.getInt("deux_points");
        int deuxPointsReussis = rs.getInt("deux_points_reussis");
        int troisPoints = rs.getInt("trois_points");
        int troisPointsReussis = rs.getInt("trois_points_reussis");
        int rebonds = rs.getInt("rebonds");
        int fautes = rs.getInt("fautes");
        int passesDecisives = rs.getInt("passes_decisives");
        int ballonsPerdus = rs.getInt("ballons_perdus");
        int interceptions = rs.getInt("interceptions");
        int contres = rs.getInt("contres");
        return new BilanJoueur(j, lancerFrancs, lancerFrancsReussis, deuxPoints, deuxPointsReussis, troisPoints, troisPointsReussis, rebonds, fautes, passesDecisives, ballonsPerdus, interceptions, contres);
    }

    /**
     * Méthode permettant d'accéder à l'instance de {@link BilansDao}
     * @return l'instance de {@link BilansDao}
     */
    public static BilansDao getInstance() {
        return instance;
    }
}
