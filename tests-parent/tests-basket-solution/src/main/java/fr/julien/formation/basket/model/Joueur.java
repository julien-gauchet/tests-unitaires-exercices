package fr.julien.formation.basket.model;

/**
 * Classe permettant de définir un joueur de basket
 * @author Julien Gauchet
 */
public class Joueur {

    private String nom;
    private String prenom;
    private Equipe equipe;

    /**
     * Constructeur de {@link Joueur}
     * @param nom
     *            Le nom du joueur
     * @param prenom
     *            Le prénom du joueur
     * @param equipe
     *            L'équipe du joueur
     */
    public Joueur(String nom, String prenom, Equipe equipe) {
        super();
        this.nom = nom;
        this.prenom = prenom;
        this.equipe = equipe;
    }

    /**
     * Méthode permettant d'accéder au nom du joueur
     * @return le nom du joueur
     */
    public String getNom() {
        return nom;
    }

    /**
     * Méthode permettant d'accéder au prenom du joueur
     * @return le prenom du joueur
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * Méthode permettant d'accéder à l'équipe du joueur
     * @return l'équipe du joueur
     */
    public Equipe getEquipe() {
        return equipe;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((equipe == null) ? 0 : equipe.hashCode());
        result = prime * result + ((nom == null) ? 0 : nom.hashCode());
        result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Joueur)) {
            return false;
        }
        Joueur other = (Joueur) obj;
        if (equipe == null) {
            if (other.equipe != null) {
                return false;
            }
        }
        else if (!equipe.equals(other.equipe)) {
            return false;
        }
        if (nom == null) {
            if (other.nom != null) {
                return false;
            }
        }
        else if (!nom.equalsIgnoreCase(other.nom)) {
            return false;
        }
        if (prenom == null) {
            if (other.prenom != null) {
                return false;
            }
        }
        else if (!prenom.equalsIgnoreCase(other.prenom)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return prenom + " " + nom;
    }
}
