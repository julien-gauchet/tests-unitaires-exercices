package fr.julien.formation.basket.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe permettant de définir un match
 * @author Julien Gauchet
 */
public class Match {

    private List<BilanJoueur> bilans;

    /**
     * Constructeur de {@link Match}
     */
    public Match() {
        this.bilans = new ArrayList<>();
    }

    /**
     * Constructeur de {@link Match}
     * @param bilans
     *            La listes des bilans des joueurs
     */
    public Match(List<BilanJoueur> bilans) {
        this.bilans = bilans;
    }

    /**
     * Méthode permettant d'ajouter un bilan
     * @param bilan
     */
    public void addBilan(BilanJoueur bilan) {
        this.bilans.add(bilan);
    }

    /**
     * Méthode permettant d'accéder aux bilans des joueurs
     * @return les bilans des joueurs
     */
    public List<BilanJoueur> getBilans() {
        return bilans;
    }
}
