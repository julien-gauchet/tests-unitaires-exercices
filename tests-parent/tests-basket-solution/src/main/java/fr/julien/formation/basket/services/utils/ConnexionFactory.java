package fr.julien.formation.basket.services.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import fr.julien.formation.basket.exception.BasketException;

/**
 * Factory permettant de générer des connexions
 * @author Julien Gauchet
 */
public class ConnexionFactory {

    private static ConnexionFactory instance = new ConnexionFactory();

    private ConnexionFactory() {
        super();
    }

    /**
     * Méthode pemrettant de générer une connexion
     * @param driverClassName
     *            Le nom du driver
     * @param url
     *            L'url de connexion
     * @param username
     *            Le user de connexion
     * @param password
     *            Le mot de passe associé
     * @return la connexion
     * @throws BasketException
     */
    public Connection generer(String driverClassName, String url, String username, String password) throws BasketException {
        Connection res = null;
        try {
            Class.forName(driverClassName);
            res = DriverManager.getConnection(url, username, password);
            try (Statement stmt = res.createStatement()) {
                stmt.executeUpdate("set local enable_nestloop=off");
                stmt.executeUpdate("set local enable_hashjoin=on");
                stmt.executeUpdate("set local enable_mergejoin=off");
            }
        }
        catch (ClassNotFoundException | SQLException e) {
            throw new BasketException(e.getMessage(), e);
        }
        return res;
    }

    /**
     * Méthode permettant d'accéder à l'instance de {@link ConnexionFactory}
     * @return l'instance de {@link ConnexionFactory}
     */
    public static ConnexionFactory getInstance() {
        return instance;
    }
}
