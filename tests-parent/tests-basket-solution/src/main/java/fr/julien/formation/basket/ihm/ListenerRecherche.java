package fr.julien.formation.basket.ihm;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import fr.julien.formation.basket.exception.BasketException;
import fr.julien.formation.basket.model.TableauMatch;
import fr.julien.formation.basket.services.statistiques.GenerationTableauMatch;

/**
 * {@link ActionListener} permettant d'afficher le résultat d'une recherche de match dans la base de données
 * 
 * @author Julien Gauchet
 */
public class ListenerRecherche implements ActionListener {

    private TableauExtensible tableauEquipe1;
    private TableauExtensible tableauEquipe2;
    private JTextField journee;
    private Connection cnx;

    /**
     * Constructeur de {@link ListenerRecherche}
     * 
     * @param cnx
     *     La connexion à la base de données
     * @param tableauEquipe1
     *     Le tableau de l'équipe 1
     * @param tableauEquipe2
     *     Le tableau de l'équipe 2
     * @param journee
     *     Le numéro de la jourée concernée
     */
    public ListenerRecherche(Connection cnx, TableauExtensible tableauEquipe1, TableauExtensible tableauEquipe2, JTextField journee) {
        this.tableauEquipe1 = tableauEquipe1;
        this.tableauEquipe2 = tableauEquipe2;
        this.journee = journee;
        this.cnx = cnx;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            TableauMatch tableau = GenerationTableauMatch.getInstance().genererTableauMatch(cnx, Integer.parseInt(journee.getText()));
            tableauEquipe1.getEquipe().setText(" " + tableau.getTableauEquipe1().getNomEquipe());
            tableauEquipe2.getEquipe().setText(" " + tableau.getTableauEquipe2().getNomEquipe());
            for (List<String> l : tableau.getTableauEquipe1().getStatistiques()) {
                tableauEquipe1.ajouter(l.toArray());
            }
            for (List<String> l : tableau.getTableauEquipe2().getStatistiques()) {
                tableauEquipe2.ajouter(l.toArray());
            }
        }
        catch (NumberFormatException | BasketException e1) {
            JOptionPane.showMessageDialog(null, "Impossible de charger les données demandées", "Erreur lors du chargment", JOptionPane.ERROR_MESSAGE);
        }
    }
}
