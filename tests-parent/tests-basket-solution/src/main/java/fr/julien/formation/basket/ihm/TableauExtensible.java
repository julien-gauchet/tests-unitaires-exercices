package fr.julien.formation.basket.ihm;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * Classe définissant un tableau extensible.
 * @author Julien Gauchet
 */
public class TableauExtensible extends JPanel {

    private static final long serialVersionUID = -7294611737481736871L;
    private JTable table;
    private JLabel equipe;

    /**
     * Constructeur de tableau extensible
     * @param entetes
     *            Les entetes du tableau
     */
    public TableauExtensible(final Object[] entetes) {
        Object[][] data = new Object[0][entetes.length];
        this.table = new JTable(new DefaultTableModel(data, entetes));
        table.getColumnModel().getColumn(0).setPreferredWidth(150);
        this.equipe = new JLabel(" ");
        setLayout(new BorderLayout());
        add(new JScrollPane(table), BorderLayout.CENTER);
        add(equipe, BorderLayout.NORTH);
    }

    /**
     * Méthode permettant d'accéder au equipe
     * @return le equipe
     */
    public JLabel getEquipe() {
        return equipe;
    }

    /**
     * Méthode permettant d'ajouter une ligne à tableau
     * @param ligne
     *            Le tableau contenant les différentes colonnes à ajouter
     */
    public void ajouter(Object[] ligne) {
        ((DefaultTableModel) (table.getModel())).addRow(ligne);
    }

    /**
     * Méthode permettant de vider le tableau
     */
    public void vider() {
        ((DefaultTableModel) (table.getModel())).setRowCount(0);
    }
}
