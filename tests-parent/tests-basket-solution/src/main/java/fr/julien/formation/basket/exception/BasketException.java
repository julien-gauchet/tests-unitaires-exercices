package fr.julien.formation.basket.exception;

/**
 * Classe d'exception associée à ce module
 * @author Julien Gauchet
 */
public class BasketException extends Exception {

    private static final long serialVersionUID = -8062952049136579055L;

    /**
     * Constructeur de {@link BasketException}
     * @param message
     *            Le message d'erreur
     * @param cause
     *            L'exception mère
     */
    public BasketException(String message, Throwable cause) {
        super(message, cause);
    }
}
