package fr.julien.formation.basket.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe permettant de définir le tableau de l'équipe
 * 
 * @author Julien Gauchet
 */
public class TableauEquipe {

    private String nomEquipe;
    private List<List<String>> statistiques;

    public TableauEquipe() {
        this.statistiques = new ArrayList<>();
    }

    public TableauEquipe(String nomEquipe) {
        this.nomEquipe = nomEquipe;
        this.statistiques = new ArrayList<>();
    }

    /**
     * Méthode permettant d'accéder au nom de l'équipe
     * 
     * @return le nom de l'équipe
     */
    public String getNomEquipe() {
        return nomEquipe;
    }

    /**
     * Méthode permettant d'accéder aux statistiques
     * 
     * @return les statistiques
     */
    public List<List<String>> getStatistiques() {
        return statistiques;
    }

    public void setNomEquipe(String nomEquipe) {
        this.nomEquipe = nomEquipe;
    }

    public void setStatistiques(List<List<String>> statistiques) {
        this.statistiques = statistiques;
    }
}