package fr.julien.formation.basket.model;

/**
 * Classe permettant de définir le tableau de match
 * 
 * @author Julien Gauchet
 */
public class TableauMatch {

    private TableauEquipe tableauEquipe1;
    private TableauEquipe tableauEquipe2;

    public TableauMatch(TableauEquipe tableauEquipe1, TableauEquipe tableauEquipe2) {
        super();
        this.tableauEquipe1 = tableauEquipe1;
        this.tableauEquipe2 = tableauEquipe2;
    }

    /**
     * Méthode permettant d'accéder au tableau de l'équipe 1
     * 
     * @return le tableau de l'équipe 1
     */
    public TableauEquipe getTableauEquipe1() {
        return tableauEquipe1;
    }

    /**
     * Méthode permettant d'accéder tableau de l'équipe 2
     * 
     * @return le tableau de l'équipe 2
     */
    public TableauEquipe getTableauEquipe2() {
        return tableauEquipe2;
    }
}
