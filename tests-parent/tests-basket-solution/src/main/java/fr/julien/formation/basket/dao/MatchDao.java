package fr.julien.formation.basket.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import fr.julien.formation.basket.exception.BasketException;

/**
 * Classe permettant d'accéder à la table matchs
 * @author Julien Gauchet
 */
public class MatchDao {

    private static MatchDao instance = new MatchDao();
    private static final String REQUETE_INSERTION = "INSERT INTO matchs (id) VALUES (?)";

    private MatchDao() {
        super();
    }

    /**
     * Méthode permettant d'insérer un match dans la base
     * @param cnx
     *            La connexion à la base de données
     * @param b
     *            Le bilan à insérer
     * @param idMatch
     *            L'identifiant du match
     * @throws BasketException
     */
    public void inserer(Connection cnx, int idMatch) throws BasketException {
        try (PreparedStatement stmt = cnx.prepareStatement(REQUETE_INSERTION)) {
            int cpt = 1;
            stmt.setInt(cpt, idMatch);
            stmt.executeUpdate();
        }
        catch (SQLException e) {
            throw new BasketException(e.getMessage(), e);
        }
    }

    /**
     * Méthode permettant d'accéder à l'instance de {@link MatchDao}
     * @return l'instance de {@link MatchDao}
     */
    public static MatchDao getInstance() {
        return instance;
    }
}
