package fr.julien.formation.basket.services.statistiques;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import fr.julien.formation.basket.dao.BilansDao;
import fr.julien.formation.basket.exception.BasketException;
import fr.julien.formation.basket.model.BilanJoueur;
import fr.julien.formation.basket.model.TableauEquipe;
import fr.julien.formation.basket.model.TableauMatch;

public class GenerationTableauMatch {

    private static GenerationTableauMatch instance = new GenerationTableauMatch();

    private GenerationTableauMatch() {
        super();
    }

    public TableauMatch genererTableauMatch(Connection cnx, int journee) throws BasketException {
        List<BilanJoueur> bilans = BilansDao.getInstance().getBilansByMatch(cnx, journee);
        String equipe1 = null;
        String equipe2 = null;
        TableauEquipe tableauEquipe1 = new TableauEquipe();
        TableauEquipe tableauEquipe2 = new TableauEquipe();
        for (BilanJoueur b : bilans) {
            if (equipe1 == null) {
                equipe1 = b.getJoueur().getEquipe().getNom();
                tableauEquipe1.setNomEquipe(equipe1);
            }
            else if (equipe2 == null && !b.getJoueur().getEquipe().getNom().equalsIgnoreCase(equipe1)) {
                equipe2 = b.getJoueur().getEquipe().getNom();
                tableauEquipe2.setNomEquipe(equipe2);
            }
            if (b.getJoueur().getEquipe().getNom().equals(equipe1)) {
                tableauEquipe1.getStatistiques().add(calculerScore(b));
            }
            else if (b.getJoueur().getEquipe().getNom().equals(equipe2)) {
                tableauEquipe2.getStatistiques().add(calculerScore(b));
            }
        }
        return new TableauMatch(tableauEquipe1, tableauEquipe2);
    }

    private List<String> calculerScore(BilanJoueur b) {
        List<String> stat = new ArrayList<>();
        int score = CalculStatistiquesService.getInstance().calculerScore(b);
        stat.add(b.getJoueur().toString());
        stat.add(String.valueOf(score));
        stat.add(b.getDeuxPointsReussis() + "/" + b.getDeuxPoints());
        stat.add(b.getTroisPointsReussis() + "/" + b.getTroisPoints());
        stat.add(b.getLancerFrancsReussis() + "/" + b.getLancerFrancs());
        stat.add(String.valueOf(b.getRebonds()));
        stat.add(String.valueOf(b.getPassesDecisives()));
        stat.add(String.valueOf(b.getFautes()));
        stat.add(String.valueOf(b.getInterceptions()));
        stat.add(String.valueOf(b.getBallonsPerdus()));
        stat.add(String.valueOf(b.getContres()));
        stat.add(String.valueOf(CalculStatistiquesService.getInstance().calculerEvaluation(b)));
        return stat;
    }

    public static GenerationTableauMatch getInstance() {
        return instance;
    }
}
