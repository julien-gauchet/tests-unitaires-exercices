INSERT INTO equipes(id, nom) 
VALUES 
	(1, 'Orléans'), 
	(2, 'Limoges');
INSERT INTO joueurs (id, nom, prenom, id_equipe)
VALUES 
	(201, 'CAMARA', 'Ousmane', 2),
	(202, 'JONES', 'Joseph', 2),
	(203, 'PREPELIC', 'Klemen', 2),
	(204, 'WOJCIECHOWSKI', 'Mathieu', 2),
	(205, 'WOOD', 'Dashaun', 2),
	(206, 'DELAGE', 'Benjamin', 2),
	(207, 'DUPORT', 'Romain', 2),
	(208, 'FAIR', 'C.J', 2),
	(209, 'MUNANGA', 'Shekinah', 2),
	(210, 'RANDLE', 'Jerome', 2),
	(211, 'ZERBO', 'Fréjus', 2),
	(101, 'DOWNS', 'Micah', 1),
	(102, 'LOUBAKI', 'Luc', 1),
	(103, 'MILOSEVIC', 'Nemanja', 1),
	(104, 'McALARNEY', 'Kyle', 1),
	(105, 'OLASENI', 'Gabe', 1),
	(106, 'JOSEPH', 'Georgi', 1),
	(107, 'MENDY', 'Antoine', 1),
	(108, 'PRINCE', 'John', 1),
	(109, 'SOMMERVILLE', 'Marcellus', 1),
	(110, 'SYLLA', 'Abdel Kader', 1),
	(111, 'VINCENT', 'Thomas', 1);
	
INSERT INTO matchs(id) VALUES (34);

INSERT INTO bilans (id_match, id_joueur, lancer_francs_reussis, lancer_francs, deux_points_reussis, deux_points, trois_points_reussis, trois_points, rebonds, fautes, passes_decisives,ballons_perdus, interceptions, contres)
VALUES 
	(34, 201, 0, 0, 6, 14, 0, 0, 8, 3, 1, 1, 0, 0),
	(34, 202, 0, 1, 2, 3, 0, 0, 0, 2, 1, 1, 0, 0),
	(34, 203, 4, 4, 0, 3, 3, 9, 1, 4, 5, 1, 0, 0),
	(34, 204, 0, 0, 0, 0, 2, 2, 8, 4, 2, 1, 0, 0),
	(34, 205, 0, 0, 2, 4, 1, 7, 2, 3, 2, 3, 3, 0),
	(34, 206, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	(34, 207, 0, 0, 1, 3, 1, 3, 2, 0, 0, 0, 0, 0),
	(34, 208, 2, 4, 3, 4, 0, 3, 5, 1, 0, 0, 1, 1),
	(34, 209, 0, 0, 2, 3, 1, 1, 3, 2, 0, 1, 1, 0),
	(34, 210, 0, 0, 4, 7, 2, 4, 1, 0, 3, 4, 0, 0),
	(34, 211, 3, 4, 1, 3, 0, 0, 1, 0, 0, 0, 1, 0),
	(34, 101, 1, 2, 2, 2, 3, 5, 5, 0, 4, 1, 0, 0),
	(34, 102, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0),
	(34, 103, 0, 0, 2, 2, 1, 2, 1, 1, 1, 0, 0, 0),
	(34, 104, 3, 3, 0, 2, 1, 4, 3, 1, 2, 1, 1, 0),
	(34, 105, 2, 2, 4, 10, 0, 0, 6, 2, 2, 0, 1, 0),
	(34, 106, 0, 0, 1, 1, 1, 1, 3, 2, 1, 1, 1, 0),
	(34, 107, 1, 1, 5, 7, 2, 7, 3, 0, 2, 1, 1, 0),
	(34, 108, 1, 2, 7, 8, 1, 2, 5, 2, 10, 2, 4, 0),
	(34, 109, 4, 4, 4, 6, 0, 1, 3, 3, 3, 2, 0, 0),
	(34, 110, 0, 1, 5, 7, 0, 0, 5, 2, 4, 2, 2, 0),
	(34, 111, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0);