package fr.julien.formation.basket.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class JoueurTest {
    @Test
    public void testerEquals() {
	Joueur kyle = new Joueur("MCALARNEY", "Kyle", new Equipe("OLB"));
	Joueur kyle2 = new Joueur("MCALARNEY", "Kyle", new Equipe("OLB"));
	Joueur kyle3 = new Joueur("MCALARNEY", "Kyle", new Equipe("Monaco"));
	Joueur kyle4 = new Joueur("MCALARNEY", "Autre", new Equipe("OLB"));
	Joueur cellus = new Joueur("SOMMERVILLE", "Marcellus", new Equipe("OLB"));
	Joueur cellus2 = new Joueur("SOMMERVILLE", "Kyle", new Equipe("OLB"));
	Assertions.assertEquals(true, kyle.equals(kyle2));
	Assertions.assertEquals(false, kyle.equals(kyle3));
	Assertions.assertEquals(false, kyle.equals(kyle4));
	Assertions.assertEquals(false, kyle.equals(cellus));
	Assertions.assertEquals(false, kyle.equals(cellus2));
    }
}
