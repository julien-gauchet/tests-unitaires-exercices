package fr.julien.formation.basket.services.matchs.easymock;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.easymock.EasyMock;
import org.h2.jdbcx.JdbcDataSource;
import org.h2.tools.RunScript;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import fr.julien.formation.basket.model.Equipe;
import fr.julien.formation.basket.services.lnb.AccesWebServiceLNB;
import fr.julien.formation.basket.services.matchs.MiseAJourMatchs;

@DisplayName("Test des mises à jour avec Mockito")
public class MiseAJourMatchsTest {
    private Connection cnx;

    @BeforeEach
    public void setUp() {
	JdbcDataSource dataSource = new JdbcDataSource();
	dataSource.setURL("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1");
	dataSource.setUser("sa");
	dataSource.setPassword("");
	try {
	    cnx = dataSource.getConnection();
	    try (InputStreamReader fr = new FileReader(new File("src/test/resources/init_database_test.sql")); BufferedReader br = new BufferedReader(fr)) {
		RunScript.execute(cnx, br);
	    }
	}
	catch (Exception e) {
	    e.printStackTrace();
	}
    }

    @AfterEach
    public void tearDown() {
	try {
	    cnx.close();
	}
	catch (SQLException e) {
	    e.printStackTrace();
	}
    }

    @Test
    @DisplayName("Test global")
    public void testerMiseAJour() {
	try {
	    AccesWebServiceLNB webService = EasyMock.createNiceMock(AccesWebServiceLNB.class);
	    StringBuilder sb = new StringBuilder();
	    sb.append("<matchs journee=\"34\">");
	    sb.append("SOMMERVILLE;Marcellus;Orléans;5;3;5;5;2;1;3;2;3;1;0;1");
	    sb.append("</matchs>");
	    EasyMock.expect(webService.getReponse("http://www.lnb.fr/ws/journee/", 33)).andReturn(sb.toString());
	    EasyMock.replay(webService);
	    MiseAJourMatchs.getInstance().setAccesWebServiceLNB(webService);
	    MiseAJourMatchs.getInstance().setAccesWebServiceLNB(webService);
	    MiseAJourMatchs.getInstance().mettreAJour(cnx, new Equipe("Orléans"), 33);
	    try (Statement stmt = cnx.createStatement()) {
		try (ResultSet rs = stmt.executeQuery("SELECT * FROM bilans WHERE id_match=33")) {
		    Assertions.assertTrue(rs.next());
		    Assertions.assertEquals(5, rs.getInt("lancer_francs"));
		    Assertions.assertEquals(3, rs.getInt("lancer_francs_reussis"));
		    Assertions.assertEquals(5, rs.getInt("deux_points"));
		    Assertions.assertEquals(5, rs.getInt("deux_points_reussis"));
		    Assertions.assertEquals(2, rs.getInt("trois_points"));
		    Assertions.assertEquals(1, rs.getInt("trois_points_reussis"));
		    Assertions.assertEquals(3, rs.getInt("rebonds"));
		    Assertions.assertEquals(2, rs.getInt("fautes"));
		    Assertions.assertEquals(3, rs.getInt("passes_decisives"));
		    Assertions.assertEquals(1, rs.getInt("ballons_perdus"));
		    Assertions.assertEquals(0, rs.getInt("interceptions"));
		    Assertions.assertEquals(1, rs.getInt("contres"));
		}
	    }
	}
	catch (Exception e) {
	    e.printStackTrace();
	    Assertions.fail();
	}
    }
}