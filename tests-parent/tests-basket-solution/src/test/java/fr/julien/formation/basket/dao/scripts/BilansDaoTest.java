package fr.julien.formation.basket.dao.scripts;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.h2.jdbcx.JdbcDataSource;
import org.h2.tools.RunScript;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.julien.formation.basket.dao.BilansDao;
import fr.julien.formation.basket.exception.BasketException;
import fr.julien.formation.basket.model.BilanJoueur;
import fr.julien.formation.basket.model.Equipe;
import fr.julien.formation.basket.model.Joueur;

public class BilansDaoTest {

    private Connection cnx;

    @BeforeEach
    public void setUp() {
        JdbcDataSource dataSource = new JdbcDataSource();
        dataSource.setURL("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1");
        dataSource.setUser("sa");
        dataSource.setPassword("");
        try {
            cnx = dataSource.getConnection();
            try (InputStreamReader fr = new FileReader(new File("src/test/resources/init_database_test.sql")); BufferedReader br = new BufferedReader(fr)) {
                RunScript.execute(cnx, br);
            }
            try (InputStreamReader fr = new FileReader(new File("src/test/resources/data/BilansDaoTest.sql")); BufferedReader br = new BufferedReader(fr)) {
                RunScript.execute(cnx, br);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AfterEach
    public void tearDown() {
        try {
            cnx.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testerSelection() {
        try {
            List<BilanJoueur> bilans = BilansDao.getInstance().getBilansByMatch(cnx, 34);
            Assertions.assertEquals(22, bilans.size());
            Assertions.assertEquals(new Joueur("CAMARA", "Ousmane", new Equipe("Limoges")), bilans.get(0).getJoueur());
            Assertions.assertEquals(0, bilans.get(0).getLancerFrancsReussis());
            Assertions.assertEquals(0, bilans.get(0).getLancerFrancs());
            Assertions.assertEquals(6, bilans.get(0).getDeuxPointsReussis());
            Assertions.assertEquals(14, bilans.get(0).getDeuxPoints());
            Assertions.assertEquals(0, bilans.get(0).getTroisPointsReussis());
            Assertions.assertEquals(0, bilans.get(0).getTroisPoints());
            Assertions.assertEquals(1, bilans.get(0).getPassesDecisives());
            Assertions.assertEquals(1, bilans.get(0).getBallonsPerdus());
            Assertions.assertEquals(3, bilans.get(0).getFautes());
            Assertions.assertEquals(8, bilans.get(0).getRebonds());
            Assertions.assertEquals(0, bilans.get(0).getInterceptions());
            Assertions.assertEquals(0, bilans.get(0).getContres());
        }
        catch (BasketException e) {
            e.printStackTrace();
            Assertions.fail();
        }
    }

    @Test
    public void testerInsertion() {
        try {
            try (Statement stmt = cnx.createStatement()) {
                stmt.executeUpdate("INSERT INTO joueurs(id, nom, prenom, id_equipe) VALUES (0, 'Gauchet', 'Julien', 2)");
            }
            BilanJoueur b = new BilanJoueur(new Joueur("Gauchet", "Julien", new Equipe("Orléans")), 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
            BilansDao.getInstance().inserer(cnx, b, 34);
            try (Statement stmt = cnx.createStatement(); ResultSet rs = stmt.executeQuery("SELECT * FROM bilans WHERE id_joueur=0")) {
        	Assertions.assertTrue(rs.next());
                Assertions.assertEquals(2, rs.getInt("lancer_francs_reussis"));
            }
        }
        catch (BasketException | SQLException e) {
            e.printStackTrace();
            Assertions.fail();
        }
    }
}
