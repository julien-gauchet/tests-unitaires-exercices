package fr.julien.formation.basket.dao.dbunit;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import org.dbunit.Assertion;
import org.dbunit.assertion.comparer.value.ValueComparer;
import org.dbunit.assertion.comparer.value.ValueComparers;
import org.dbunit.assertion.comparer.value.builder.ColumnValueComparerMapBuilder;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.h2.jdbcx.JdbcDataSource;
import org.h2.tools.RunScript;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.julien.formation.basket.dao.MatchDao;

public class MatchsDaoTest {

    private Connection cnx;

    @BeforeEach
    public void setUp() {
        JdbcDataSource dataSource = new JdbcDataSource();
        dataSource.setURL("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1");
        dataSource.setUser("sa");
        dataSource.setPassword("");
        try {
            cnx = dataSource.getConnection();
            try (InputStreamReader fr = new FileReader(new File("src/test/resources/init_database_test.sql")); BufferedReader br = new BufferedReader(fr)) {
                RunScript.execute(cnx, br);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AfterEach
    public void tearDown() {
        try {
            cnx.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testerInsertion() {
        try {
            MatchDao.getInstance().inserer(cnx, 1);
            try (Statement stmt = cnx.createStatement()) {
                IDatabaseConnection dbUnitConnection = new DatabaseConnection(cnx);
                QueryDataSet partialDataSet = new QueryDataSet(dbUnitConnection);
                partialDataSet.addTable("matchs", "SELECT * FROM matchs");
                FlatXmlDataSetBuilder xmlDSBuilder = new FlatXmlDataSetBuilder();
                xmlDSBuilder.setCaseSensitiveTableNames(false);
                InputStream inputStreamXML = new FileInputStream("src/test/resources/datasets/MatchsDaoTestResultat.xml");
                IDataSet dataSet = xmlDSBuilder.build(inputStreamXML);
                ITable tableAttendue = dataSet.getTable("matchs");
                Map<String, ValueComparer> columnValueComparers = new ColumnValueComparerMapBuilder().build();
                Assertion.assertWithValueComparer(tableAttendue, partialDataSet.getTable("matchs"), ValueComparers.isActualEqualToExpected, columnValueComparers);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            Assertions.fail();
        }
    }
}
