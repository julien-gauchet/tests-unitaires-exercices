package fr.julien.formation.basket.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class EquipeTest {
    @Test
    public void testerEquals() {
	Equipe e1 = new Equipe("Orléans");
	Equipe e2 = new Equipe("Orléans");
	Equipe e3 = new Equipe("ASVEL");
	Assertions.assertEquals(true, e1.equals(e2));
	Assertions.assertEquals(false, e1.equals(e3));
    }
}
