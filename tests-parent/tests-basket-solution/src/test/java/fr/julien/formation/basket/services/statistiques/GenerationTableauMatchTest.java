package fr.julien.formation.basket.services.statistiques;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;

import org.h2.jdbcx.JdbcDataSource;
import org.h2.tools.RunScript;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.julien.formation.basket.model.TableauMatch;

public class GenerationTableauMatchTest {
    private Connection cnx;

    @BeforeEach
    public void setUp() {
	JdbcDataSource dataSource = new JdbcDataSource();
	dataSource.setURL("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1");
	dataSource.setUser("sa");
	dataSource.setPassword("");
	try {
	    cnx = dataSource.getConnection();
	    try (InputStreamReader fr = new FileReader(new File("src/test/resources/init_database_test.sql")); BufferedReader br = new BufferedReader(fr)) {
		RunScript.execute(cnx, br);
	    }
	    try (InputStreamReader fr = new FileReader(new File("src/test/resources/data/GenerationTableauMatchTest.sql")); BufferedReader br = new BufferedReader(fr)) {
		RunScript.execute(cnx, br);
	    }
	}
	catch (Exception e) {
	    e.printStackTrace();
	}
    }

    @AfterEach
    public void tearDown() {
	try {
	    cnx.close();
	}
	catch (SQLException e) {
	    e.printStackTrace();
	}
    }

    @Test
    public void testerGenerationTableaux() {
	try {
	    TableauMatch t = GenerationTableauMatch.getInstance().genererTableauMatch(cnx, 34);
	    Assertions.assertEquals(true, t.getTableauEquipe1() != null);
	    Assertions.assertEquals(true, t.getTableauEquipe2() != null);
	    Assertions.assertEquals("Limoges", t.getTableauEquipe1().getNomEquipe());
	    Assertions.assertEquals(11, t.getTableauEquipe1().getStatistiques().size());
	    int cpt = 0;
	    Assertions.assertEquals("Ousmane CAMARA", t.getTableauEquipe1().getStatistiques().get(0).get(cpt++));
	    Assertions.assertEquals("12", t.getTableauEquipe1().getStatistiques().get(0).get(cpt++));
	    Assertions.assertEquals("6/14", t.getTableauEquipe1().getStatistiques().get(0).get(cpt++));
	    Assertions.assertEquals("0/0", t.getTableauEquipe1().getStatistiques().get(0).get(cpt++));
	    Assertions.assertEquals("0/0", t.getTableauEquipe1().getStatistiques().get(0).get(cpt++));
	    Assertions.assertEquals("8", t.getTableauEquipe1().getStatistiques().get(0).get(cpt++));
	    Assertions.assertEquals("1", t.getTableauEquipe1().getStatistiques().get(0).get(cpt++));
	    Assertions.assertEquals("3", t.getTableauEquipe1().getStatistiques().get(0).get(cpt++));
	    Assertions.assertEquals("0", t.getTableauEquipe1().getStatistiques().get(0).get(cpt++));
	    Assertions.assertEquals("1", t.getTableauEquipe1().getStatistiques().get(0).get(cpt++));
	    Assertions.assertEquals("0", t.getTableauEquipe1().getStatistiques().get(0).get(cpt++));
	    Assertions.assertEquals("12", t.getTableauEquipe1().getStatistiques().get(0).get(cpt++));
	}
	catch (Exception e) {
	    e.printStackTrace();
	    Assertions.fail();
	}
    }
}