package fr.julien.formation.basket.dao.scripts;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.h2.jdbcx.JdbcDataSource;
import org.h2.tools.RunScript;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.julien.formation.basket.dao.MatchDao;

public class MatchsDaoTest {
    private Connection cnx;

    @BeforeEach
    public void setUp() {
	JdbcDataSource dataSource = new JdbcDataSource();
	dataSource.setURL("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1");
	dataSource.setUser("sa");
	dataSource.setPassword("");
	try {
	    cnx = dataSource.getConnection();
	    try (InputStreamReader fr = new FileReader(new File("src/test/resources/init_database_test.sql")); BufferedReader br = new BufferedReader(fr)) {
		RunScript.execute(cnx, br);
	    }
	    try (InputStreamReader fr = new FileReader(new File("src/test/resources/data/MatchsDaoTest.sql")); BufferedReader br = new BufferedReader(fr)) {
		RunScript.execute(cnx, br);
	    }
	}
	catch (Exception e) {
	    e.printStackTrace();
	}
    }

    @AfterEach
    public void tearDown() {
	try {
	    cnx.close();
	}
	catch (SQLException e) {
	    e.printStackTrace();
	}
    }

    @Test
    public void testerInsertion() {
	try {
	    MatchDao.getInstance().inserer(cnx, 1);
	    try (Statement stmt = cnx.createStatement()) {
		try (ResultSet rs = stmt.executeQuery("SELECT * FROM matchs WHERE id=1")) {
		    Assertions.assertTrue(rs.next());
		}
	    }
	}
	catch (Exception e) {
	    e.printStackTrace();
	    Assertions.fail();
	}
    }
}
