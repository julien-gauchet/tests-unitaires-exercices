package fr.julien.formation.basket.services.statistiques;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import fr.julien.formation.basket.model.BilanJoueur;
import fr.julien.formation.basket.model.Equipe;
import fr.julien.formation.basket.model.Joueur;

public class CalculStatistiqueServiceTest {
    @Test
    public void testerCalculScore() {
	Joueur kyle = new Joueur("MCALARNEY", "Kyle", new Equipe("OLB"));
	BilanJoueur bilan = new BilanJoueur(kyle, 1, 1, 7, 6, 7, 2, 3, 5, 2, 1, 1, 2);
	Assertions.assertEquals(19, CalculStatistiquesService.getInstance().calculerScore(bilan));
	Assertions.assertEquals(20, CalculStatistiquesService.getInstance().calculerEvaluation(bilan));
    }
}
