package fr.julien.tests.services;

import org.h2.jdbcx.JdbcDataSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import fr.julien.tests.connection.ConnectionProxy;

public class CreationJsonbTest {
    @Test
    public void testerService() {
	JdbcDataSource dataSource = new JdbcDataSource();
	dataSource.setURL("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1");
	dataSource.setUser("sa");
	dataSource.setPassword("");
	try (ConnectionProxy cnx = new ConnectionProxy(dataSource.getConnection())) {
	    cnx.setTest(true);
	    int nb = new CreationJsonb().trucAvecJsonb(cnx);
	    Assertions.assertEquals(2, nb);
	}
	catch (Exception e) {
	    e.printStackTrace();
	}
    }
}
