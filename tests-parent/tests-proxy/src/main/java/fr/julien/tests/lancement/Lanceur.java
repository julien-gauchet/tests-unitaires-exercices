package fr.julien.tests.lancement;

import org.h2.jdbcx.JdbcDataSource;

import fr.julien.tests.connection.ConnectionProxy;
import fr.julien.tests.services.CreationJsonb;

public class Lanceur {
    public static void main(String[] args) {
	JdbcDataSource dataSource = new JdbcDataSource();
	dataSource.setURL("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1");
	dataSource.setUser("sa");
	dataSource.setPassword("");
	try (ConnectionProxy cnx = new ConnectionProxy(dataSource.getConnection())) {
	    new CreationJsonb().trucAvecJsonb(cnx);
	}
	catch (Exception e) {
	    e.printStackTrace();
	}
    }
}
