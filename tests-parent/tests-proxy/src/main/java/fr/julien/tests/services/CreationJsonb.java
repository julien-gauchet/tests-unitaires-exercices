package fr.julien.tests.services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class CreationJsonb {
    public int trucAvecJsonb(Connection cnx) throws SQLException {
	try (Statement stmt = cnx.createStatement()) {
	    int res = 0;
	    stmt.executeUpdate("CREATE TABLE test(id serial, liste jsonb)");
	    stmt.executeUpdate("INSERT INTO test(liste) VALUES ('{\"val1\": 1}'), ('{\"val1\": 2}')");
	    try (ResultSet rs = stmt.executeQuery("SELECT * FROM test")) {
		while (rs.next()) {
		    res++;
		    System.out.println(rs.getInt("id") + ":" + rs.getString("liste"));
		}
	    }
	    return res;
	}
    }
}
