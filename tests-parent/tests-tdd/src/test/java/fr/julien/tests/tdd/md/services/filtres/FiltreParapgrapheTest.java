package fr.julien.tests.tdd.md.services.filtres;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.DefaultConfiguration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class FiltreParapgrapheTest {
    @BeforeAll
    public static void init() {
	Configurator.initialize(new DefaultConfiguration());
	Configurator.setRootLevel(Level.INFO);
    }

    @Test
    public void testerFiltreParagraphe() {
	List<String> lignes = Stream.of("# Titre", "Paragraphe 1", "", "Paragraphe 2").collect(Collectors.toList());
	List<String> res = FiltreParagraphe.getInstance().traiter(lignes);
	Assertions.assertEquals(3, res.size());
	Assertions.assertEquals("<p>Paragraphe 1</p>", res.get(1));
    }
}
