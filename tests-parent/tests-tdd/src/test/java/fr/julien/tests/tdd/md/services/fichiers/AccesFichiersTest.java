package fr.julien.tests.tdd.md.services.fichiers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.DefaultConfiguration;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class AccesFichiersTest {
    @BeforeAll
    public static void init() {
	Configurator.initialize(new DefaultConfiguration());
	Configurator.setRootLevel(Level.INFO);
    }

    @AfterAll
    public static void end() {
	File f = new File("src/test/resources/tmp/fichier1.txt");
	if (f.exists()) {
	    f.delete();
	}
    }

    @Test
    public void testerLecture() {
	try {
	    List<String> l = AccesFichiers.getInstance().lireFichier(new File("src/test/resources/donnees/services/fichiers/LectureFichierTest/exemple.md"));
	    Assertions.assertNotNull(l);
	    Assertions.assertEquals(11, l.size());
	}
	catch (IOException e) {
	    e.printStackTrace();
	    Assertions.fail();
	}
    }

    @Test
    public void testerEcriture() {
	try {
	    List<String> lignes = Stream.of("ligne1", "ligne2").collect(Collectors.toList());
	    File f = new File("src/test/resources/tmp/fichier1.txt");
	    AccesFichiers.getInstance().ecrireFichier(f, lignes);
	    List<String> lignesLues = Files.readAllLines(f.toPath());
	    Assertions.assertEquals(2, lignesLues.size());
	}
	catch (IOException e) {
	    e.printStackTrace();
	    Assertions.fail();
	}
    }
}
