package fr.julien.tests.tdd.md.services.traitements;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.DefaultConfiguration;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class TraductionFichiersTest {
    private static File sortie = new File("src/test/resources/tmp/sortie.html");

    @BeforeAll
    public static void init() {
	Configurator.initialize(new DefaultConfiguration());
	Configurator.setRootLevel(Level.INFO);
    }

    @AfterAll
    public static void end() {
	if (sortie.exists()) {
	    sortie.delete();
	}
    }

    @Test
    public void testerTraitement() {
	File entree = new File("src/test/resources/donnees/services/traitements/TraductionFichiersTest/exemple.md");
	try {
	    TraductionFichiers.getInstance().traduireFichier(entree, sortie);
	    Assertions.assertTrue(sortie.exists());
	    List<String> lignes = Files.readAllLines(sortie.toPath());
	    Assertions.assertNotNull(lignes);
	    Assertions.assertEquals(6, lignes.size());
	    List<String> attendu = Stream.of(
		    "<h1>Titre de premier niveau</h1>", 
		    "<p>premier paragraphe</p>", 
		    "<h1>Titre de premier niveau</h1>", 
		    "<h2>Titre de second niveau</h2>", 
		    "<p>Paragraphe 1</p>", 
		    "<p>Paragraphe 2</p>").collect(Collectors.toList());
	    Assertions.assertEquals(attendu, lignes);
	}
	catch (IOException e) {
	    e.printStackTrace();
	    Assertions.fail();
	}
    }
}
