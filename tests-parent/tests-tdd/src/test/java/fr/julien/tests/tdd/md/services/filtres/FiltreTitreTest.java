package fr.julien.tests.tdd.md.services.filtres;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.DefaultConfiguration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class FiltreTitreTest {
    @BeforeAll
    public static void init() {
	Configurator.initialize(new DefaultConfiguration());
	Configurator.setRootLevel(Level.INFO);
    }

    @Test
    public void testerFiltreTitre() {
	List<String> lignes = Stream.of("# Titre", "## Titre 2", "Paragraphe 1", "", "Paragraphe 2").collect(Collectors.toList());
	List<String> res = FiltreTitre.getInstance().traiter(lignes);
	Assertions.assertEquals(5, res.size());
	Assertions.assertEquals("<h1>Titre</h1>", res.get(0));
	Assertions.assertEquals("<h2>Titre 2</h2>", res.get(1));
    }
}
