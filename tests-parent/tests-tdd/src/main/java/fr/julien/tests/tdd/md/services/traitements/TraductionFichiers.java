package fr.julien.tests.tdd.md.services.traitements;

import java.io.File;
import java.io.IOException;
import java.util.List;

import fr.julien.tests.tdd.md.services.fichiers.AccesFichiers;
import fr.julien.tests.tdd.md.services.filtres.FiltreParagraphe;
import fr.julien.tests.tdd.md.services.filtres.FiltreTitre;

public class TraductionFichiers {
    private static final TraductionFichiers instance = new TraductionFichiers();

    private TraductionFichiers() {
	super();
    }

    public List<String> traduireFichier(File entree, File sortie) throws IOException {
	List<String> lignes = AccesFichiers.getInstance().lireFichier(entree);
	lignes = FiltreTitre.getInstance().traiter(lignes);
	lignes = FiltreParagraphe.getInstance().traiter(lignes);
	AccesFichiers.getInstance().ecrireFichier(sortie, lignes);
	return lignes;
    }

    public static TraductionFichiers getInstance() {
	return instance;
    }
}
