package fr.julien.tests.tdd.md.services.filtres;

import java.util.ArrayList;
import java.util.List;

public class FiltreTitre implements Filtre {
    private static final FiltreTitre instance = new FiltreTitre();

    private FiltreTitre() {
	super();
    }

    @Override
    public List<String> traiter(List<String> entree) {
	List<String> res = new ArrayList<>();
	for (String s : entree) {
	    if (s.startsWith("# ")) {
		res.add("<h1>" + s.substring(2) + "</h1>");
	    }
	    else if (s.startsWith("## ")) {
		res.add("<h2>" + s.substring(3) + "</h2>");
	    }
	    else {
		res.add(s);
	    }
	}
	return res;
    }

    public static FiltreTitre getInstance() {
	return instance;
    }
}
