package fr.julien.tests.tdd.md.services.fichiers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AccesFichiers {
    private static final Logger LOG = LogManager.getLogger(AccesFichiers.class);
    private static final AccesFichiers instance = new AccesFichiers();
    
    private AccesFichiers() {
	super();
    }

    public List<String> lireFichier(File f) throws IOException {
	LOG.info("Lecture du fichier " + f.toString());
	return Files.readAllLines(f.toPath());
    }

    public void ecrireFichier(File f, List<String> contenu) throws IOException {
	LOG.info("Ecriture du fichier " + f.toString());
	Files.write(f.toPath(), contenu);
    }

    public static AccesFichiers getInstance() {
	return instance;
    }
}
