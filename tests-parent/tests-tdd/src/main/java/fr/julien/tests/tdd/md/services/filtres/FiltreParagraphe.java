package fr.julien.tests.tdd.md.services.filtres;

import java.util.ArrayList;
import java.util.List;

public class FiltreParagraphe implements Filtre {
    private static final FiltreParagraphe instance = new FiltreParagraphe();

    private FiltreParagraphe() {
	super();
    }

    @Override
    public List<String> traiter(List<String> entree) {
	List<String> res = new ArrayList<>();
	for (int i = 0; i < entree.size(); i++) {
	    String s = entree.get(i);
	    if ((s == null || s.isEmpty())) {
		if (!res.get(res.size() - 1).startsWith("<")) {
		    res.set(res.size() - 1, "<p>" + res.get(res.size() - 1) + "</p>");
		}
	    }
	    else {
		res.add(s);
	    }
	}
	if (!res.get(res.size() - 1).startsWith("<")) {
	    res.set(res.size() - 1, "<p>" + res.get(res.size() - 1) + "</p>");
	}
	return res;
    }

    public static FiltreParagraphe getInstance() {
	return instance;
    }
}
