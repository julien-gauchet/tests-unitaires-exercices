package fr.julien.tests.tdd.md.services.filtres;

import java.util.List;

public interface Filtre {
    List<String> traiter(List<String> entree);
}
