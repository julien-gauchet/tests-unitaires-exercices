package fr.julien.graphiques.ihm;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fr.julien.graphiques.ihm.ComboColorChooserIhm.MyColor;

/**
 * 
 * Classe GestionGraphique
 * 
 * Classe qui définit les options disponibles pour les graphiques
 * 
 * @author julien
 *
 */
public class GestionGraphiqueIhm extends JPanel {

    private static final long serialVersionUID = 1L;
    /** Nuage à représenter */
    private RepresentationNuageIhm nuage;
    /** Elements graphiques */
    private JPanel panelAxes;
    private JPanel panelGraphique;
    private JPanel panelTextes;
    private JPanel panelAffichage;
    private JLabel labelOrigine = new JLabel("Origine du repère");
    private JLabel labelCouleurAxes = new JLabel("Couleur des axes");
    private JLabel labelPasX = new JLabel("Pas de l'axe X");
    private JLabel labelPasY = new JLabel("Pas de l'axe Y");
    private JLabel labelCouleurPoints = new JLabel("Couleur points");
    private JLabel labelCouleurDroite = new JLabel("Couleur droite");
    private JLabel labelTaillePoints = new JLabel("Taille points");
    private JLabel labelTitre = new JLabel("Titre");
    private JLabel labelTailleTitre = new JLabel("Taille du titre");
    private JLabel labelTailleLegendes = new JLabel("Taille légendes");
    private JLabel labelLegendeX = new JLabel("Légende x");
    private JLabel labelLegendeY = new JLabel("Légende Y");
    private JTextField textPasX;
    private JTextField textPasY;
    private JTextField textOrigineX;
    private JTextField textOrigineY;
    private JTextField textTaillePoints;
    private JTextField textTitre;
    private JTextField textTailleTitre;
    private JTextField textTailleLegendes;
    private JTextField textLegendeX;
    private JTextField textLegendeY;
    private ComboColorChooserIhm comboAxes = new ComboColorChooserIhm();
    private ComboColorChooserIhm comboPoints = new ComboColorChooserIhm();
    private ComboColorChooserIhm comboDroite = new ComboColorChooserIhm();
    private JButton validerAxes = new JButton("Valider");
    private JButton validerGraphique = new JButton("Valider");
    private JButton validerTextes = new JButton("Valider");
    private JCheckBox cboxAxes = new JCheckBox("Axes");
    private JCheckBox cboxDroite = new JCheckBox("Droite de régression");
    private JCheckBox cboxPoints = new JCheckBox("Nuage de points");
    private JButton validerAffichage = new JButton("Valider");

    public GestionGraphiqueIhm(RepresentationNuageIhm r) {
        super();
        nuage = r;
        setLayout(null);
        setBounds(10, 10, 515, 435);
        textOrigineX = new JTextField(r.getOrigineX() + "");
        textOrigineY = new JTextField(r.getOrigineY() + "");
        textPasX = new JTextField(r.getPasX() + "");
        textPasY = new JTextField(r.getPasY() + "");
        textTaillePoints = new JTextField(r.getTaillePoint() + "");
        textTitre = new JTextField(r.getTitre());
        textTailleTitre = new JTextField(r.getTailleTitre() + "");
        textLegendeX = new JTextField(r.getTextX());
        textLegendeY = new JTextField(r.getTextY());
        textTailleLegendes = new JTextField(r.getTailleTextAxes() + "");
        comboAxes.setSelectedItem(r.getCouleurAxe());
        comboDroite.setSelectedItem(r.getCouleurDroite());
        comboPoints.setSelectedItem(r.getCouleurPoint());
        cboxAxes.setSelected(r.isAfficherAxes());
        cboxDroite.setSelected(r.isAfficherDroite());
        cboxPoints.setSelected(r.isAfficherNuage());
        panelAxes = new JPanel();
        panelAxes.setLayout(null);
        panelAxes.setBounds(0, 0, 250, 220);
        panelAxes.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Paramètres des axes", 0, 0, new Font("Calibri", Font.PLAIN, 12), Color.BLACK));
        panelGraphique = new JPanel();
        panelGraphique.setLayout(null);
        panelGraphique.setBounds(0, 225, 250, 180);
        panelGraphique.setBorder(BorderFactory.createEtchedBorder());
        panelGraphique
                .setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Paramètres du graphique", 0, 0, new Font("Calibri", Font.PLAIN, 12), Color.BLACK));
        panelTextes = new JPanel();
        panelTextes.setLayout(null);
        panelTextes.setBounds(255, 0, 250, 260);
        panelTextes.setBorder(BorderFactory.createEtchedBorder());
        panelTextes.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Etiquettes", 0, 0, new Font("Calibri", Font.PLAIN, 12), Color.BLACK));
        panelAffichage = new JPanel();
        panelAffichage.setLayout(null);
        panelAffichage.setBounds(255, 265, 250, 140);
        panelAffichage.setBorder(BorderFactory.createEtchedBorder());
        panelAffichage.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Affichage", 0, 0, new Font("Calibri", Font.PLAIN, 12), Color.BLACK));
        panelAxes.add(labelOrigine);
        panelAxes.add(labelPasX);
        panelAxes.add(labelPasY);
        panelAxes.add(labelCouleurAxes);
        panelAxes.add(textOrigineX);
        panelAxes.add(textOrigineY);
        panelAxes.add(textPasX);
        panelAxes.add(comboAxes);
        panelAxes.add(textPasY);
        panelAxes.add(validerAxes);
        panelGraphique.add(labelCouleurDroite);
        panelGraphique.add(labelCouleurPoints);
        panelGraphique.add(labelTaillePoints);
        panelGraphique.add(textTaillePoints);
        panelGraphique.add(comboDroite);
        panelGraphique.add(comboPoints);
        panelGraphique.add(validerGraphique);
        panelTextes.add(labelTitre);
        panelTextes.add(labelTailleTitre);
        panelTextes.add(labelLegendeX);
        panelTextes.add(labelLegendeY);
        panelTextes.add(labelTailleLegendes);
        panelTextes.add(textTitre);
        panelTextes.add(textTailleTitre);
        panelTextes.add(textLegendeX);
        panelTextes.add(textLegendeY);
        panelTextes.add(textTailleLegendes);
        panelTextes.add(validerTextes);
        panelAffichage.add(cboxAxes);
        panelAffichage.add(cboxDroite);
        panelAffichage.add(cboxPoints);
        panelAffichage.add(validerAffichage);
        int alignementGauche = 10;
        int alignementGauche2 = 150;
        int tailleLabels = 150;
        int tailleText = 30;
        int tailleCombo = 90;
        labelOrigine.setBounds(alignementGauche, 25, tailleLabels, 25);
        labelCouleurAxes.setBounds(alignementGauche, 70, tailleLabels, 25);
        labelPasX.setBounds(alignementGauche, 110, tailleLabels, 25);
        labelPasY.setBounds(alignementGauche, 140, tailleLabels, 25);
        textOrigineX.setBounds(alignementGauche2, 25, tailleText, 25);
        textOrigineY.setBounds(alignementGauche2 + 35, 25, tailleText, 25);
        comboAxes.setBounds(alignementGauche2, 70, tailleCombo, 25);
        textPasX.setBounds(alignementGauche2, 110, tailleText, 25);
        textPasY.setBounds(alignementGauche2, 140, tailleText, 25);
        validerAxes.setBounds(50, 180, 150, 30);
        labelCouleurDroite.setBounds(alignementGauche, 25, tailleLabels, 25);
        labelCouleurPoints.setBounds(alignementGauche, 60, tailleLabels, 25);
        labelTaillePoints.setBounds(alignementGauche, 100, tailleLabels, 25);
        textTaillePoints.setBounds(alignementGauche2, 100, tailleText, 25);
        comboDroite.setBounds(alignementGauche2, 25, tailleCombo, 25);
        comboPoints.setBounds(alignementGauche2, 60, tailleCombo, 25);
        validerGraphique.setBounds(50, 140, 150, 30);
        labelTitre.setBounds(alignementGauche, 25, tailleLabels, 25);
        labelTailleTitre.setBounds(alignementGauche, 60, tailleLabels, 25);
        labelLegendeX.setBounds(alignementGauche, 110, tailleLabels, 25);
        labelLegendeY.setBounds(alignementGauche, 145, tailleLabels, 25);
        labelTailleLegendes.setBounds(alignementGauche, 180, tailleLabels, 25);
        textTitre.setBounds(alignementGauche2, 25, tailleCombo, 25);
        textTailleTitre.setBounds(alignementGauche2, 60, tailleText, 25);
        textLegendeX.setBounds(alignementGauche2, 110, tailleCombo, 25);
        textLegendeY.setBounds(alignementGauche2, 145, tailleCombo, 25);
        textTailleLegendes.setBounds(alignementGauche2, 180, tailleText, 25);
        validerTextes.setBounds(50, 220, 150, 30);
        cboxAxes.setBounds(alignementGauche, 25, 190, 20);
        cboxDroite.setBounds(alignementGauche, 45, 190, 20);
        cboxPoints.setBounds(alignementGauche, 65, 190, 20);
        validerAffichage.setBounds(50, 100, 150, 30);
        validerAxes.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                nuage.setOrigineX(Double.parseDouble(textOrigineX.getText()));
                nuage.setOrigineY(Double.parseDouble(textOrigineY.getText()));
                nuage.setCouleurAxe((MyColor) comboAxes.getSelectedItem());
                nuage.setPasX(Double.parseDouble(textPasX.getText()));
                nuage.setPasY(Double.parseDouble(textPasY.getText()));
                nuage.repaint();
            }
        });
        validerGraphique.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                nuage.setTaillePoint(Integer.parseInt(textTaillePoints.getText()));
                nuage.setCouleurDroite((MyColor) comboDroite.getSelectedItem());
                nuage.setCouleurPoint((MyColor) comboPoints.getSelectedItem());
                nuage.repaint();
            }
        });
        validerTextes.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                nuage.setTitre(textTitre.getText());
                nuage.setTailleTitre(Integer.parseInt(textTailleTitre.getText()));
                nuage.setTailleTextAxes(Integer.parseInt(textTailleLegendes.getText()));
                nuage.setTextX(textLegendeX.getText());
                nuage.setTextY(textLegendeY.getText());
                nuage.repaint();
            }
        });
        validerAffichage.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                nuage.setAfficherAxes(cboxAxes.isSelected());
                nuage.setAfficherDroite(cboxDroite.isSelected());
                nuage.setAfficherNuage(cboxPoints.isSelected());
                nuage.repaint();
                for (Component c : panelAxes.getComponents()) {
                    c.setEnabled(cboxAxes.isSelected());
                }
                if (!cboxAxes.isSelected() && (textLegendeX.getText().length() > 0 || textLegendeY.getText().length() > 0)) {
                    nuage.setTextX("");
                    nuage.setTextY("");
                }
                else {
                    nuage.setTextX(textLegendeX.getText());
                    nuage.setTextY(textLegendeY.getText());
                }
                textLegendeX.setEnabled(cboxAxes.isSelected());
                textLegendeY.setEnabled(cboxAxes.isSelected());
                textTailleLegendes.setEnabled(cboxAxes.isSelected());
                nuage.repaint();
                comboDroite.setEnabled(cboxDroite.isSelected());
                comboPoints.setEnabled(cboxPoints.isSelected());
                textTaillePoints.setEnabled(cboxPoints.isSelected());
            }
        });
        add(panelAxes);
        add(panelGraphique);
        add(panelTextes);
        add(panelAffichage);
    }
}
