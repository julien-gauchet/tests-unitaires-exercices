package fr.julien.graphiques.lancement;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.julien.graphiques.exception.StatistiqueException;

public class LanceurGraphique {

    public static void main(String[] args) throws StatistiqueException {
        try (AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(ConfigGraphique.class)) {
            Initialisation doc = applicationContext.getBean(Initialisation.class);
            doc.init();
        }
    }
}
