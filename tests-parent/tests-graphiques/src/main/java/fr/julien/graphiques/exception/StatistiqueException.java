package fr.julien.graphiques.exception;

/**
 * 
 * Classe TechnicalException
 * 
 * Classe qui définit un type d'exception
 * 
 * @author julien
 *
 */
public class StatistiqueException extends Exception {

    private static final long serialVersionUID = 1L;

    public StatistiqueException() {
        super();
    }

    public StatistiqueException(String msg) {
        super(msg);
    }

    public StatistiqueException(String msg, Throwable e) {
        super(msg, e);
    }

    public StatistiqueException(Throwable e) {
        super(e);
    }
}
