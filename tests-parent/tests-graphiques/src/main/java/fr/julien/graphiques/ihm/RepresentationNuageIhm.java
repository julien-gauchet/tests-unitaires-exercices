package fr.julien.graphiques.ihm;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;

import javax.swing.JPanel;

import fr.julien.graphiques.modele.dto.ResultatRegression;
import fr.julien.graphiques.modele.dto.Vecteur;

/**
 * 
 * Classe RepresentationNuage
 * 
 * Classe qui définit la fenêtre de graphique
 * 
 * @author julien
 *
 */
public class RepresentationNuageIhm extends JPanel {

    private static final long serialVersionUID = 1L;
    /** Options de représentation */
    private int taillePoint = 4;
    private Color couleurFont = Color.white;
    private Color couleurPoint = Color.black;
    private Color couleurAxe = Color.black;
    private Color couleurDroite = Color.red;

    private double origineX = 0;
    private double origineY = 0;
    private double pasX = 1;
    private double pasY = 1;
    private int tailleGrad = 10;
    private String titre = "Titre";
    private String textX = "x";
    private String textY = "y";
    private boolean afficherNuage = true;
    private boolean afficherAxes = true;
    private boolean afficherDroite = true;
    private int textXx = -1;
    private int textXy = -1;
    private int textYx = -1;
    private int textYy = -1;
    private int titrex = -1;
    private int titrey = 20;
    private int tailleTitre = 16;
    private int tailleTextAxes = 11;
    private Color couleurTitre = Color.black;
    private Color couleurTextAxes = couleurAxe;
    private Font fTitre = new Font("Calibri", Font.BOLD, tailleTitre);
    private Font fTextAxes = new Font("Calibri", Font.BOLD, tailleTextAxes);
    private ResultatRegression resultatRegression;

    public RepresentationNuageIhm(ResultatRegression resultatRegression) {
        super();

        this.resultatRegression = resultatRegression;
    }

    protected void paintComponent(Graphics g) {
        g.setColor(couleurFont);
        g.fillRect(getX(), getY(), getWidth(), getHeight());
        if (afficherAxes) {
            g.setColor(couleurAxe);
            dessinerAxes(g);
            dessinerEchelles(g);
        }
        if (afficherNuage) {
            g.setColor(couleurPoint);
            for (int k = 0; k < resultatRegression.getNuage().getX().size(); k++) {
                double x = resultatRegression.getNuage().getX().get(k);
                double y = resultatRegression.getNuage().getY().get(k);
                g.fillOval((int) (-taillePoint / 2 + convertX(x)), (int) (-taillePoint / 2 + convertY(y)), taillePoint, taillePoint);
            }
        }
        if (afficherDroite) {
            g.setColor(couleurDroite);
            dessinerDroiteRegression(g);
        }
        dessinerTexte(g);
    }

    /**
     * Fonction qui permet de dessiner les axes
     * 
     * @param g
     */
    private void dessinerAxes(Graphics g) {
        int w = this.getWidth();
        int h = this.getHeight();
        g.drawLine((int) (convertX(origineX)), 30, (int) (convertX(origineX)), h - 30);
        g.drawLine(30, (int) (convertY(origineY)), w - 30, (int) (convertY(origineY)));
    }

    /**
     * Fonction qui ajoute un titre et les légendes
     * 
     * @param g
     */
    private void dessinerTexte(Graphics g) {
        g.setFont(fTitre);
        g.setColor(couleurTitre);
        FontMetrics fm = g.getFontMetrics();
        setTitrex((getWidth() - fm.stringWidth(getTitre())) / 2);
        g.drawString(getTitre(), getTitrex(), getTitrey());
        g.setFont(fTextAxes);
        g.setColor(couleurTextAxes);
        fm = g.getFontMetrics();
        setTextXx((getWidth() - fm.stringWidth(getTextX())) / 2);
        setTextXy((int) convertY(origineY) + 40);
        g.drawString(getTextX(), getTextXx(), getTextXy());
        fm = g.getFontMetrics();
        setTextYx(((int) convertX(origineX) - fm.stringWidth(getTextY())) - 30);
        setTextYy(-5 + getHeight() / 2);
        g.drawString(getTextY(), getTextYx(), getTextYy());
    }

    private double max(Vecteur v) {
        double max = v.get(0);
        for (int i = 1; i < v.size(); i++) {
            if (v.get(i) > max) {
                max = v.get(i);
            }
        }
        return max;
    }

    private double min(Vecteur v) {
        double min = v.get(0);
        for (int i = 1; i < v.size(); i++) {
            if (v.get(i) < min) {
                min = v.get(i);
            }
        }
        return min;
    }

    /**
     * Fonction qui trace les échelles
     * 
     * @param g
     */
    private void dessinerEchelles(Graphics g) {
        int Xmax = Math.max((int) (max(resultatRegression.getNuage().getX()) + 1), 0);
        int Xmin = Math.min((int) (min(resultatRegression.getNuage().getX()) - 1), 0);
        int Ymax = Math.max((int) (max(resultatRegression.getNuage().getY()) + 1), 0);
        int Ymin = Math.min((int) (min(resultatRegression.getNuage().getY()) - 1), 0);
        double o = origineX;
        while (convertX(o) < convertX(Xmax)) {
            g.drawLine((int) (convertX(o)), (int) convertY(origineY), (int) (convertX(o)), (int) convertY(origineY) + tailleGrad);
            g.drawString(o + "", (int) (convertX(o)) - 3, (int) (convertY(origineY) + tailleGrad + 12));
            o += pasX;
        }
        o = origineX - pasX;
        while (convertX(o) > convertX(Xmin)) {
            g.drawLine((int) (convertX(o)), (int) convertY(origineY), (int) (convertX(o)), (int) convertY(origineY) + tailleGrad);
            g.drawString(o + "", (int) (convertX(o)) - 7, (int) (convertY(origineY) + tailleGrad + 12));
            o -= pasX;
        }
        double oy = origineY;
        while (convertY(oy) > convertY(Ymax)) {
            g.drawLine((int) convertX(origineX), (int) (convertY(oy)), (int) convertX(origineX) - tailleGrad, (int) (convertY(oy)));
            g.drawString(oy + "", (int) (convertX(origineX) - tailleGrad - 12), (int) (convertY(oy)) - 3);
            oy += pasY;
        }
        oy = origineY - pasY;
        while (convertY(oy) < convertY(Ymin)) {
            g.drawLine((int) convertX(origineX), (int) (convertY(oy)), (int) convertX(origineX) - tailleGrad, (int) (convertY(oy)));
            g.drawString(oy + "", (int) (convertX(origineX) - tailleGrad - 12), (int) (convertY(oy)) - 3);
            oy -= pasY;
        }
    }

    /**
     * Fonction qui ajoute une droite de régression
     * 
     * @param g
     */
    private void dessinerDroiteRegression(Graphics g) {
        int Xmax = Math.max((int) (max(resultatRegression.getNuage().getX()) + 1), 0);
        int Xmin = Math.min((int) (min(resultatRegression.getNuage().getX()) - 1), 0);
        g.drawLine((int) (convertX(Xmin)), (int) (convertY(resultatRegression.getB0() + resultatRegression.getB1() * Xmin)), (int) (convertX(Xmax)),
                (int) (convertY(resultatRegression.getB0() + resultatRegression.getB1() * Xmax)));
    }

    /**
     * Fonction qui permet de passer des coordonnées du graphique
     * aux coordonnées de la fenetre pour les abscisses
     * 
     * @param x
     *     : abscisse du graphique
     * @return : abscisse de la fenetre
     */
    private double convertX(double x) {
        int w = this.getWidth();
        int Xmax = Math.max((int) (max(resultatRegression.getNuage().getX()) + 1), 0);
        int Xmin = Math.min((int) (min(resultatRegression.getNuage().getX()) - 1), 0);
        return (x - Xmin) * w / (Xmax - Xmin);
    }

    /**
     * Fonction qui permet de passer des coordonnées du graphique
     * aux coordonnées de la fenetre pour les ordonnées
     * 
     * @param y
     *     : ordonnée du graphique
     * @return : ordonnée de la fenetre
     */
    private double convertY(double y) {
        int h = this.getHeight();
        int Ymax = Math.max((int) (max(resultatRegression.getNuage().getY()) + 1), 0);
        int Ymin = Math.min((int) (min(resultatRegression.getNuage().getY()) - 1), 0);
        return h - (y - Ymin) * h / (Ymax - Ymin);
    }

    public int getTaillePoint() {
        return taillePoint;
    }

    public void setTaillePoint(int taillePoint) {
        this.taillePoint = taillePoint;
    }

    public Color getCouleurFont() {
        return couleurFont;
    }

    public void setCouleurFont(Color couleurFont) {
        this.couleurFont = couleurFont;
    }

    public Color getCouleurPoint() {
        return couleurPoint;
    }

    public void setCouleurPoint(Color couleurPoint) {
        this.couleurPoint = couleurPoint;
    }

    public Color getCouleurAxe() {
        return couleurAxe;
    }

    public void setCouleurAxe(Color couleurAxe) {
        this.couleurAxe = couleurAxe;
    }

    public Color getCouleurDroite() {
        return couleurDroite;
    }

    public void setCouleurDroite(Color couleurDroite) {
        this.couleurDroite = couleurDroite;
    }

    public double getOrigineX() {
        return origineX;
    }

    public void setOrigineX(double origineX) {
        this.origineX = origineX;
    }

    public double getOrigineY() {
        return origineY;
    }

    public void setOrigineY(double origineY) {
        this.origineY = origineY;
    }

    public double getPasX() {
        return pasX;
    }

    public void setPasX(double pasX) {
        this.pasX = pasX;
    }

    public double getPasY() {
        return pasY;
    }

    public void setPasY(double pasY) {
        this.pasY = pasY;
    }

    public int getTailleGrad() {
        return tailleGrad;
    }

    public void setTailleGrad(int tailleGrad) {
        this.tailleGrad = tailleGrad;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getTextX() {
        return textX;
    }

    public void setTextX(String textX) {
        this.textX = textX;
    }

    public String getTextY() {
        return textY;
    }

    public void setTextY(String textY) {
        this.textY = textY;
    }

    public int getTextXx() {
        return textXx;
    }

    public void setTextXx(int textXx) {
        this.textXx = textXx;
    }

    public int getTextXy() {
        return textXy;
    }

    public void setTextXy(int textXy) {
        this.textXy = textXy;
    }

    public int getTextYx() {
        return textYx;
    }

    public void setTextYx(int textYx) {
        this.textYx = textYx;
    }

    public int getTextYy() {
        return textYy;
    }

    public void setTextYy(int textYy) {
        this.textYy = textYy;
    }

    public int getTitrex() {
        return titrex;
    }

    public void setTitrex(int titrex) {
        this.titrex = titrex;
    }

    public int getTitrey() {
        return titrey;
    }

    public void setTitrey(int titrey) {
        this.titrey = titrey;
    }

    public int getTailleTitre() {
        return tailleTitre;
    }

    public void setTailleTitre(int tailleTitre) {
        this.tailleTitre = tailleTitre;
    }

    public int getTailleTextAxes() {
        return tailleTextAxes;
    }

    public void setTailleTextAxes(int tailleTextAxes) {
        this.tailleTextAxes = tailleTextAxes;
    }

    public Color getCouleurTitre() {
        return couleurTitre;
    }

    public void setCouleurTitre(Color couleurTitre) {
        this.couleurTitre = couleurTitre;
    }

    public Color getCouleurTextAxes() {
        return couleurTextAxes;
    }

    public void setCouleurTextAxes(Color couleurTextAxes) {
        this.couleurTextAxes = couleurTextAxes;
    }

    public boolean isAfficherNuage() {
        return afficherNuage;
    }

    public void setAfficherNuage(boolean afficherNuage) {
        this.afficherNuage = afficherNuage;
    }

    public boolean isAfficherAxes() {
        return afficherAxes;
    }

    public void setAfficherAxes(boolean afficherAxes) {
        this.afficherAxes = afficherAxes;
    }

    public boolean isAfficherDroite() {
        return afficherDroite;
    }

    public void setAfficherDroite(boolean afficherDroite) {
        this.afficherDroite = afficherDroite;
    }
}
