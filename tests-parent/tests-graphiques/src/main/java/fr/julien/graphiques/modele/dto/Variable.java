package fr.julien.graphiques.modele.dto;

/**
 * 
 * Classe Variable
 * 
 * Classe qui définit une variable
 * 
 * @author julien
 *
 */
public class Variable {

    /** Nom de la variable */
    private String nom = "inconnu";
    /** Valeurs prises */
    private Vecteur valeurs;
    /** Descriptif de la variable */
    private String descriptif = "aucun";

    public Variable() {
        setValeurs(new Vecteur());
    }

    public Variable(String nom) {
        setNom(nom);
        setValeurs(new Vecteur());
    }

    public Variable(String nom, String d) {
        setNom(nom);
        setValeurs(new Vecteur());
        setDescriptif(d);
    }

    public Variable(String nom, Vecteur valeurs) {
        setNom(nom);
        setValeurs(valeurs);
    }

    public Variable(String nom, Vecteur valeurs, String d) {
        setNom(nom);
        setValeurs(valeurs);
        setDescriptif(d);
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Vecteur getValeurs() {
        return valeurs;
    }

    public void setValeurs(Vecteur valeurs) {
        this.valeurs = valeurs;
    }

    public String getDescriptif() {
        return descriptif;
    }

    public void setDescriptif(String descriptif) {
        this.descriptif = descriptif;
    }
}
