package fr.julien.graphiques.modele.dto;

import java.util.ArrayList;
import java.util.List;

public class NuagePoints {

    private String abscisses;
    private String ordonnees;
    private List<Point> points;

    public NuagePoints(String abscisses, String ordonnees) {
        this.abscisses = abscisses;
        this.ordonnees = ordonnees;
        this.points = new ArrayList<>();
    }

    public Vecteur getX() {
        Vecteur v = new Vecteur();
        for (Point p : points) {
            v.add(p.getX());
        }
        return v;
    }

    public Vecteur getY() {
        Vecteur v = new Vecteur();
        for (Point p : points) {
            v.add(p.getY());
        }
        return v;
    }

    public void add(Point p) {
        this.points.add(p);
    }

    public String getAbscisses() {
        return abscisses;
    }

    public void setAbscisses(String abscisses) {
        this.abscisses = abscisses;
    }

    public String getOrdonnees() {
        return ordonnees;
    }

    public void setOrdonnees(String ordonnees) {
        this.ordonnees = ordonnees;
    }
}
