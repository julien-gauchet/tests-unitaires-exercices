package fr.julien.graphiques.lancement;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.h2.jdbcx.JdbcDataSource;
import org.h2.tools.RunScript;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.julien.graphiques.ihm.GestionGraphiqueIhm;
import fr.julien.graphiques.ihm.RepresentationNuageIhm;
import fr.julien.graphiques.services.GenerationRegressionService;

@Component
public class Initialisation {

    @Autowired
    private GenerationRegressionService generationRegressionService;

    public void init() {
        try {
            initDb();
            RepresentationNuageIhm n = new RepresentationNuageIhm(generationRegressionService.genererRegression());
            n.setTitre("Concentration en nickel par rapport à la concentration en Cobalt");
            GestionGraphiqueIhm g = new GestionGraphiqueIhm(n);
            JFrame f = new JFrame("Graphique");
            JPanel pg = new JPanel();
            pg.setLayout(null);
            pg.setBackground(Color.white);
            pg.add(n);
            f.setLayout(null);
            n.setBounds(0, 0, g.getWidth(), g.getHeight());
            pg.setBounds(20, 20, g.getWidth(), g.getHeight());
            g.setBounds(n.getWidth() + 40, 30, g.getWidth(), g.getHeight());
            f.setBounds(40, 40, 2 * g.getWidth() + 60, g.getHeight() + 60);
            f.add(pg);
            f.add(g);
            f.setResizable(false);
            f.setVisible(true);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initDb() throws SQLException, IOException, FileNotFoundException {
        JdbcDataSource dataSource = new JdbcDataSource();
        dataSource.setURL("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1");
        dataSource.setUser("sa");
        dataSource.setPassword("");
        Connection cnx = dataSource.getConnection();
        try (InputStreamReader fr = new FileReader(new File("src/main/resources/init_db.sql")); BufferedReader br = new BufferedReader(fr)) {
            RunScript.execute(cnx, br);
        }
    }
}
