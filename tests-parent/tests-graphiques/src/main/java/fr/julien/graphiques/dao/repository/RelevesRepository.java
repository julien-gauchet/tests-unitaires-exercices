package fr.julien.graphiques.dao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.julien.graphiques.modele.entites.Releve;

public interface RelevesRepository extends JpaRepository<Releve, Integer> {

    @Query("SELECT r FROM Releve r")
    public List<Releve> findStatistiques();
}
