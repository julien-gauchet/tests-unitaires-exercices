package fr.julien.graphiques.services.maths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.julien.graphiques.exception.StatistiqueException;
import fr.julien.graphiques.modele.dto.NuagePoints;
import fr.julien.graphiques.modele.dto.ResultatRegression;
import fr.julien.graphiques.modele.dto.Vecteur;

@Component
public class RegressionLineaireService {

    @Autowired
    private StatistiquesService statistiquesService;
    @Autowired
    private VecteursService vecteursService;

    public ResultatRegression calculer(NuagePoints nuage) throws StatistiqueException {
        Vecteur unite = new Vecteur();
        for (int i = 0; i < nuage.getX().size(); i++) {
            unite.add(1.0);
        }
        ResultatRegression resultat = new ResultatRegression();
        resultat.setB1(vecteursService.produit(
                (vecteursService.transposer(vecteursService.ajouter(nuage.getX(), vecteursService.multiplier(unite, -statistiquesService.moyenne(nuage.getX()))))), nuage.getY())
                / (statistiquesService.variance(nuage.getX()) * nuage.getX().size()));
        resultat.setB0(statistiquesService.moyenne(nuage.getY()) - statistiquesService.moyenne(nuage.getX()) * resultat.getB1());
        double r = 0;
        for (int i = 0; i < nuage.getX().size(); i++) {
            r += (nuage.getY().get(i) - (resultat.getB0() + resultat.getB1() * nuage.getX().get(i)))
                    * (nuage.getY().get(i) - (resultat.getB0() + resultat.getB1() * nuage.getX().get(i)));
        }
        resultat.setSommeCarresResiduels(r);
        resultat.setSommeCarres(statistiquesService.variance(nuage.getY()) * nuage.getY().size());
        resultat.setSommeCarresExpliques(resultat.getSommeCarres() - resultat.getSommeCarresResiduels());
        resultat.setR2(resultat.getSommeCarresExpliques() / resultat.getSommeCarres());
        resultat.setSigma(resultat.getSommeCarresResiduels() / (nuage.getX().size() - 2));
        resultat.setCoefficientDeterminationAjuste(resultat.getR2() - ((1 - resultat.getR2())) / (nuage.getX().size() - 2));
        return resultat;
    }
}
