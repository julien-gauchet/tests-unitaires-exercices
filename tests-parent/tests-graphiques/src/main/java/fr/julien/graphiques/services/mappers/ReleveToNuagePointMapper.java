package fr.julien.graphiques.services.mappers;

import java.util.List;

import org.springframework.stereotype.Component;

import fr.julien.graphiques.modele.dto.NuagePoints;
import fr.julien.graphiques.modele.dto.Point;
import fr.julien.graphiques.modele.entites.Releve;

@Component
public class ReleveToNuagePointMapper {

    public NuagePoints releveToNuagePoint(List<Releve> releves) {
        NuagePoints nuage = new NuagePoints(null, null);
        for (Releve re : releves) {
            nuage.add(new Point(re.getNi(), re.getCo()));
        }
        return nuage;
    }
}
