package fr.julien.graphiques.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.julien.graphiques.dao.repository.RelevesRepository;
import fr.julien.graphiques.exception.StatistiqueException;
import fr.julien.graphiques.modele.dto.NuagePoints;
import fr.julien.graphiques.modele.dto.ResultatRegression;
import fr.julien.graphiques.modele.entites.Releve;
import fr.julien.graphiques.services.mappers.ReleveToNuagePointMapper;
import fr.julien.graphiques.services.maths.RegressionLineaireService;

@Component
public class GenerationRegressionService {

    @Autowired
    private RelevesRepository relevesRepository;
    @Autowired
    private ReleveToNuagePointMapper releveToNuagePointMapper;
    @Autowired
    private RegressionLineaireService regressionLineaireService;

    public ResultatRegression genererRegression() throws StatistiqueException {
        List<Releve> r = relevesRepository.findStatistiques();
        NuagePoints nuage = releveToNuagePointMapper.releveToNuagePoint(r);
        ResultatRegression res = regressionLineaireService.calculer(nuage);
        res.setNuage(nuage);
        return res;
    }
}
