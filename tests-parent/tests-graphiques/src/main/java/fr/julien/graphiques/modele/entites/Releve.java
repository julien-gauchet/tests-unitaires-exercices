package fr.julien.graphiques.modele.entites;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "releves")
public class Releve implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    private Integer id;
    @Column(name = "cd")
    private Double cd;
    @Column(name = "co")
    private Double co;
    @Column(name = "cr")
    private Double cr;
    @Column(name = "cu")
    private Double cu;
    @Column(name = "ni")
    private Double ni;
    @Column(name = "pb")
    private Double pb;
    @Column(name = "zn")
    private Double zn;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getCd() {
        return cd;
    }

    public void setCd(Double cd) {
        this.cd = cd;
    }

    public Double getCo() {
        return co;
    }

    public void setCo(Double co) {
        this.co = co;
    }

    public Double getCr() {
        return cr;
    }

    public void setCr(Double cr) {
        this.cr = cr;
    }

    public Double getCu() {
        return cu;
    }

    public void setCu(Double cu) {
        this.cu = cu;
    }

    public Double getNi() {
        return ni;
    }

    public void setNi(Double ni) {
        this.ni = ni;
    }

    public Double getPb() {
        return pb;
    }

    public void setPb(Double pb) {
        this.pb = pb;
    }

    public Double getZn() {
        return zn;
    }

    public void setZn(Double zn) {
        this.zn = zn;
    }
}