DROP TABLE IF EXISTS bilans CASCADE;
DROP TABLE IF EXISTS joueurs CASCADE;
DROP TABLE IF EXISTS equipes CASCADE;
DROP TABLE IF EXISTS matchs CASCADE;

CREATE TABLE matchs (
	id INT PRIMARY KEY
);

CREATE TABLE equipes (
	id INT PRIMARY KEY,
	nom VARCHAR
);

CREATE TABLE joueurs (
	id INT PRIMARY KEY,
	nom VARCHAR,
	prenom VARCHAR,
	id_equipe INT REFERENCES equipes(id)
);

CREATE TABLE bilans (
	id_match INT,
	id_joueur INT REFERENCES joueurs(id),
	lancer_francs INT,
	lancer_francs_reussis INT,
	deux_points INT,
	deux_points_reussis INT,
	trois_points INT,
	trois_points_reussis INT,
	rebonds INT,
	fautes INT,
	passes_decisives INT,
	ballons_perdus INT,
	interceptions INT,
	contres INT
);