package fr.julien.formation.basket.services.statistiques;

import fr.julien.formation.basket.model.BilanJoueur;

/**
 * Service permettant de calculer des statistiques sur les match
 * @author Julien Gauchet
 */
public class CalculStatistiquesService {

    private static CalculStatistiquesService instance = new CalculStatistiquesService();

    private CalculStatistiquesService() {
        super();
    }

    /**
     * Méthode permettant de calculer le nombre de points marqués par un joueur
     * @param bilan
     *            Le bilan du joueur sur le match
     * @return le score du joueur
     */
    public int calculerScore(BilanJoueur bilan) {
        int score = 0;
        score += bilan.getDeuxPointsReussis() * 2;
        score += bilan.getTroisPointsReussis() * 3;
        score += bilan.getLancerFrancsReussis();
        return score;
    }

    /**
     * Méthode permettant de calculer l'évaluation d'un joueur sur un match
     * @param b
     *            Le bilan du joueur
     * @return l'évaluation
     */
    public int calculerEvaluation(BilanJoueur b) {
        int eval = 0;
        eval += calculerScore(b);
        eval += b.getRebonds();
        eval += b.getPassesDecisives();
        eval += b.getInterceptions();
        eval += b.getContres();
        eval += b.getDeuxPointsReussis();
        eval += b.getTroisPointsReussis();
        eval += b.getLancerFrancsReussis();
        eval -= b.getDeuxPoints();
        eval -= b.getTroisPoints();
        eval -= b.getLancerFrancs();
        eval -= b.getBallonsPerdus();
        return eval;
    }

    /**
     * Méthode permettant d'accéder à l'instance de {@link CalculStatistiquesService}
     * @return l'instance de {@link CalculStatistiquesService}
     */
    public static CalculStatistiquesService getInstance() {
        return instance;
    }
}
