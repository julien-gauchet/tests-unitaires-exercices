package fr.julien.formation.basket.services.matchs;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.julien.formation.basket.dao.BilansDao;
import fr.julien.formation.basket.exception.BasketException;
import fr.julien.formation.basket.model.BilanJoueur;
import fr.julien.formation.basket.model.Equipe;
import fr.julien.formation.basket.model.Joueur;
import fr.julien.formation.basket.services.lnb.AccesWebServiceLNB;
import fr.julien.formation.basket.services.lnb.AccesWebServiceLNBImpl;

/**
 * Service permettant d'insérer dans la base de données les nouveaux match en accédant à un web service mis en place par la LNB.
 * Ce web service n'est accessible qu'en production
 * 
 * @author Julien Gauchet
 */
public class MiseAJourMatchs {

    private static MiseAJourMatchs instance = new MiseAJourMatchs();

    private MiseAJourMatchs() {
        accesWebServiceLNB = AccesWebServiceLNBImpl.getInstance();
    }

    private AccesWebServiceLNB accesWebServiceLNB;

    /**
     * <p>
     * Cette méthode permet d'insérer dans la base les matchs d'une journée donnée en accédant au web service de la LNB. La réponse est sous la forme :
     * </p>
     * 
     * <pre>
     * &lt;matchs journee="??"&gt;
     * nomJoueur;prenomJoueur;nomEquipe;lancerFrancs;lancerFrancsReussis;deuxPoints;deuxPointsReussis;troisPoints;troisPointsReussis;rebonds;fautes;passesDecisives;ballonsPerdus;interceptions;contres
     * nomJoueur;prenomJoueur;nomEquipe;lancerFrancs;lancerFrancsReussis;deuxPoints;deuxPointsReussis;troisPoints;troisPointsReussis;rebonds;fautes;passesDecisives;ballonsPerdus;interceptions;contres
     * &lt;/matchs&gt;
     * </pre>
     * 
     * @param cnx
     * @param equipe
     * @param journee
     * @throws BasketException
     */
    public void mettreAJour(Connection cnx, Equipe equipe, int journee) throws BasketException {
        String reponse = accesWebServiceLNB.getReponse("http://www.lnb.fr/ws/journee/", journee);
        String[] in = reponse.substring(20, reponse.length() - 9).split(";");
        List<String> infos = new ArrayList<>();
        infos.addAll(Arrays.asList(in));
        while (!infos.isEmpty()) {
            BilanJoueur b = new BilanJoueur(new Joueur(infos.remove(0), infos.remove(0), new Equipe(infos.remove(0))), Integer.parseInt(infos.remove(0)),
                    Integer.parseInt(infos.remove(0)), Integer.parseInt(infos.remove(0)), Integer.parseInt(infos.remove(0)), Integer.parseInt(infos.remove(0)),
                    Integer.parseInt(infos.remove(0)), Integer.parseInt(infos.remove(0)), Integer.parseInt(infos.remove(0)), Integer.parseInt(infos.remove(0)),
                    Integer.parseInt(infos.remove(0)), Integer.parseInt(infos.remove(0)), Integer.parseInt(infos.remove(0)));
            BilansDao.getInstance().inserer(cnx, b, journee);
        }
    }

    /**
     * Méthode permettant d'accéder à l'instance de {@link MiseAJourMatchs}
     * 
     * @return l'instance de {@link MiseAJourMatchs}
     */
    public static MiseAJourMatchs getInstance() {
        return instance;
    }

    public void setAccesWebServiceLNB(AccesWebServiceLNB accesWebServiceLNB) {
        this.accesWebServiceLNB = accesWebServiceLNB;
    }
}
