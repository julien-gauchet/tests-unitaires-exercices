package fr.julien.formation.basket.services.lnb;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import fr.julien.formation.basket.exception.BasketException;

public class AccesWebServiceLNBImpl implements AccesWebServiceLNB {

    private static AccesWebServiceLNB instance = new AccesWebServiceLNBImpl();

    private AccesWebServiceLNBImpl() {
        super();
    }

    /**
     * Méthode permettant d'accéder au web service de la LNB
     * 
     * @param url
     *     l'url du web service
     * @param journee
     *     La journée recherchée
     * @return la réponse au format xml
     * @throws BasketException
     */
    @Override
    public String getReponse(String url, int journee) throws BasketException {
        String res = null;
        try {
            HttpGet httpget = new HttpGet(url + journee);
            HttpClient httpclient = HttpClientBuilder.create().build();
            HttpResponse response = httpclient.execute(httpget);
            res = EntityUtils.toString(response.getEntity());
        }
        catch (IOException e) {
            throw new BasketException(e.getMessage(), e);
        }
        return res;
    }

    /**
     * Méthode permettant d'accéder à l'instance de {@link WebServiceLNBProxy}
     * 
     * @return l'instance de {@link WebServiceLNBProxy}
     */
    public static AccesWebServiceLNB getInstance() {
        return instance;
    }
}
