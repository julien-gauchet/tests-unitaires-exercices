package fr.julien.formation.basket.ihm;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import fr.julien.formation.basket.dao.BilansDao;
import fr.julien.formation.basket.exception.BasketException;
import fr.julien.formation.basket.model.BilanJoueur;
import fr.julien.formation.basket.services.statistiques.CalculStatistiquesService;

/**
 * {@link ActionListener} permettant d'afficher le résultat d'une recherche de match dans la base de données
 * @author Julien Gauchet
 */
public class ListenerRecherche implements ActionListener {

    private TableauExtensible tableauEquipe1;
    private TableauExtensible tableauEquipe2;
    private JTextField journee;
    private Connection cnx;

    /**
     * Constructeur de {@link ListenerRecherche}
     * @param cnx
     *            La connexion à la base de données
     * @param tableauEquipe1
     *            Le tableau de l'équipe 1
     * @param tableauEquipe2
     *            Le tableau de l'équipe 2
     * @param journee
     *            Le numéro de la jourée concernée
     */
    public ListenerRecherche(Connection cnx, TableauExtensible tableauEquipe1, TableauExtensible tableauEquipe2, JTextField journee) {
        this.tableauEquipe1 = tableauEquipe1;
        this.tableauEquipe2 = tableauEquipe2;
        this.journee = journee;
        this.cnx = cnx;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            List<BilanJoueur> bilans = BilansDao.getInstance().getBilansByMatch(cnx, Integer.parseInt(journee.getText()));
            String equipe1 = null;
            String equipe2 = null;
            for (BilanJoueur b : bilans) {
                if (equipe1 == null) {
                    equipe1 = b.getJoueur().getEquipe().getNom();
                    tableauEquipe1.getEquipe().setText(" " + equipe1);
                }
                else if (equipe2 == null && !b.getJoueur().getEquipe().getNom().equalsIgnoreCase(equipe1)) {
                    equipe2 = b.getJoueur().getEquipe().getNom();
                    tableauEquipe2.getEquipe().setText(" " + equipe2);
                }
                if (b.getJoueur().getEquipe().getNom().equals(equipe1)) {
                    tableauEquipe1.ajouter(calculerScore(b).toArray());
                }
                else if (b.getJoueur().getEquipe().getNom().equals(equipe2)) {
                    tableauEquipe2.ajouter(calculerScore(b).toArray());
                }
            }
        }
        catch (NumberFormatException | BasketException e1) {
            JOptionPane.showMessageDialog(null, "Impossible de charger les données demandées", "Erreur lors du chargment", JOptionPane.ERROR_MESSAGE);
        }
    }

    private List<String> calculerScore(BilanJoueur b) {
        List<String> stat = new ArrayList<>();
        int score = CalculStatistiquesService.getInstance().calculerScore(b);
        stat.add(b.getJoueur().toString());
        stat.add(String.valueOf(score));
        stat.add(b.getDeuxPointsReussis() + "/" + b.getDeuxPoints());
        stat.add(b.getTroisPointsReussis() + "/" + b.getTroisPoints());
        stat.add(b.getLancerFrancsReussis() + "/" + b.getLancerFrancs());
        stat.add(String.valueOf(b.getRebonds()));
        stat.add(String.valueOf(b.getPassesDecisives()));
        stat.add(String.valueOf(b.getFautes()));
        stat.add(String.valueOf(b.getInterceptions()));
        stat.add(String.valueOf(b.getBallonsPerdus()));
        stat.add(String.valueOf(b.getContres()));
        stat.add(String.valueOf(CalculStatistiquesService.getInstance().calculerEvaluation(b)));
        return stat;
    }
}
