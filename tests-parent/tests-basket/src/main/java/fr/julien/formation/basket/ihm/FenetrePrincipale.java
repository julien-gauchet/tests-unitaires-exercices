package fr.julien.formation.basket.ihm;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.sql.Connection;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Classe permettant de définir la fenêtre principale de l'application
 * @author Julien Gauchet
 */
public class FenetrePrincipale extends JFrame {

    private static final long serialVersionUID = 9151767166431302361L;
    private JTextField match;
    private JButton valider;
    private TableauExtensible tableauEquipe1;
    private TableauExtensible tableauEquipe2;

    /**
     * Constructeur de {@link FenetrePrincipale}
     * @param cnx
     *            La connexion à la base de données
     */
    public FenetrePrincipale(Connection cnx) {
        setTitle("Match de la saison de PRO A d'Orléans");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        build();
        valider.addActionListener(new ListenerRecherche(cnx, tableauEquipe1, tableauEquipe2, match));
        setSize(900, 700);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void build() {
        String[] titres = new String[] { "Joueur", "Pts", "2pts", "3pts", "LF", "Rb.", "P.Déc.", "Fau.", "Int.", "BP.", "Ctr.", "Eva." };
        tableauEquipe1 = new TableauExtensible(titres);
        tableauEquipe2 = new TableauExtensible(titres);
        setLayout(new BorderLayout());
        JPanel nord = new JPanel(new FlowLayout());
        nord.add(new JLabel("Journée"));
        match = new JTextField();
        match.setPreferredSize(new Dimension(60, 20));
        nord.add(match);
        add(nord, BorderLayout.NORTH);
        JPanel centre = new JPanel(new BorderLayout());
        JPanel panelValidation = new JPanel(new FlowLayout());
        valider = new JButton("Rechercher");
        panelValidation.add(valider);
        centre.add(panelValidation, BorderLayout.NORTH);
        tableauEquipe1.setPreferredSize(new Dimension(80, 100));
        tableauEquipe1.setMinimumSize(tableauEquipe1.getPreferredSize());
        tableauEquipe1.setSize(tableauEquipe1.getPreferredSize());
        tableauEquipe2.setPreferredSize(new Dimension(80, 100));
        tableauEquipe2.setMinimumSize(tableauEquipe2.getPreferredSize());
        tableauEquipe2.setSize(tableauEquipe2.getPreferredSize());
        JPanel panelTableau = new JPanel(new GridLayout(0, 1));
        panelTableau.add(tableauEquipe1);
        panelTableau.add(tableauEquipe2);
        centre.add(panelTableau, BorderLayout.CENTER);
        add(centre, BorderLayout.CENTER);
    }
}
