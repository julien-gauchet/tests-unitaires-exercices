package fr.julien.formation.basket.lancement;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.sql.Connection;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.h2.jdbcx.JdbcDataSource;
import org.h2.tools.RunScript;

import fr.julien.formation.basket.ihm.FenetrePrincipale;

/**
 * Classe pemrettant d'afficher l'ihm de l'application
 * @author Julien Gauchet
 */
public final class LanceurIhm {

    private LanceurIhm() {
        super();
    }

    /**
     * Méthode permettant de lancer l'affichage de l'ihm
     * @param args
     *            tableau inutilisé
     */
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        JdbcDataSource dataSource = new JdbcDataSource();
        dataSource.setURL("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1");
        dataSource.setUser("sa");
        dataSource.setPassword("");
        try {
            Connection cnx = dataSource.getConnection();
            try (InputStreamReader fr = new FileReader(new File("src/main/resources/init_database.sql")); BufferedReader br = new BufferedReader(fr)) {
                RunScript.execute(cnx, br);
            }
            new FenetrePrincipale(cnx);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
