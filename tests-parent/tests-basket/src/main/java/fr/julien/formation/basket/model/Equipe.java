package fr.julien.formation.basket.model;

/**
 * Classe définissant une équipe de basket
 * @author Julien Gauchet
 */
public class Equipe {

    private String nom;

    /**
     * Constructeur de {@link Equipe}
     * @param nom
     *            Le nom de l'équipe
     */
    public Equipe(String nom) {
        this.nom = nom;
    }

    /**
     * Méthode permettant d'accéder au nom de l'équipe
     * @return le nom de l'équipe
     */
    public String getNom() {
        return nom;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((nom == null) ? 0 : nom.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Equipe)) {
            return false;
        }
        Equipe other = (Equipe) obj;
        if (nom == null) {
            if (other.nom != null) {
                return false;
            }
        }
        else if (!nom.equalsIgnoreCase(other.nom)) {
            return false;
        }
        return true;
    }
}
