package fr.julien.formation.basket.services.lnb;

import fr.julien.formation.basket.exception.BasketException;

public interface AccesWebServiceLNB {

    /**
     * Méthode permettant d'accéder au web service de la LNB
     * 
     * @param url
     *     l'url du web service
     * @param journee
     *     La journée recherchée
     * @return la réponse au format xml
     * @throws BasketException
     */
    String getReponse(String url, int journee) throws BasketException;
}