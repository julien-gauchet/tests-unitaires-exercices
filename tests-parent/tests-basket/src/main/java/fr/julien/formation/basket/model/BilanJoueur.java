package fr.julien.formation.basket.model;

/**
 * Classe permettant definir le bilan d'un joueur sur un match
 * @author Julien Gauchet
 */
public class BilanJoueur {

    private Joueur joueur;
    private int lancerFrancs;
    private int lancerFrancsReussis;
    private int deuxPoints;
    private int deuxPointsReussis;
    private int troisPoints;
    private int troisPointsReussis;
    private int rebonds;
    private int fautes;
    private int passesDecisives;
    private int ballonsPerdus;
    private int interceptions;
    private int contres;

    /**
     * Constructeur de {@link BilanJoueur}
     * @param joueur
     *            Le joueur concerné
     * @param lancerFrancs
     *            Le nombre de lancer francs obtenus
     * @param lancerFrancsReussis
     *            Le nmbre de lancer francs réussis
     * @param deuxPoints
     *            Le nombre de deux points tentés
     * @param deuxPointsReussis
     *            Le nombre de deux points réussis
     * @param troisPoints
     *            Le nombre de trois points tentés
     * @param troisPointsReussis
     *            Le nombre de trois points réussis
     * @param rebonds
     *            Le nombre de rebonds défensifs et offensifs
     * @param fautes
     *            Le nombre de fautes
     * @param passesDecisives
     *            Le nombre de passes décisives
     * @param ballonsPerdus
     *            Le nombre de ballons perdus
     * @param interceptions
     *            Le nombre d'interceptions
     * @param contres
     *            Le nombre de contres
     */
    public BilanJoueur(Joueur joueur, int lancerFrancs, int lancerFrancsReussis, int deuxPoints, int deuxPointsReussis, int troisPoints, int troisPointsReussis, int rebonds, int fautes, int passesDecisives, int ballonsPerdus, int interceptions, int contres) {
        this.joueur = joueur;
        this.lancerFrancs = lancerFrancs;
        this.lancerFrancsReussis = lancerFrancsReussis;
        this.deuxPoints = deuxPoints;
        this.deuxPointsReussis = deuxPointsReussis;
        this.troisPoints = troisPoints;
        this.troisPointsReussis = troisPointsReussis;
        this.rebonds = rebonds;
        this.fautes = fautes;
        this.passesDecisives = passesDecisives;
        this.ballonsPerdus = ballonsPerdus;
        this.interceptions = interceptions;
        this.contres = contres;
    }

    /**
     * Méthode permettant d'accéder au joueur
     * @return le joueur
     */
    public Joueur getJoueur() {
        return joueur;
    }

    /**
     * Méthode permettant d'accéder au nombre de lancer francs tentés
     * @return le nombre de lancer francs tentés
     */
    public int getLancerFrancs() {
        return lancerFrancs;
    }

    /**
     * Méthode permettant d'accéder au nombre de lancer francs reussis
     * @return le nombre de lancer francs reussis
     */
    public int getLancerFrancsReussis() {
        return lancerFrancsReussis;
    }

    /**
     * Méthode permettant d'accéder au nombre de deux points
     * @return le nombre de deux points
     */
    public int getDeuxPoints() {
        return deuxPoints;
    }

    /**
     * Méthode permettant d'accéder au nombre de deux points reussis
     * @return le nombre de deux points reussis
     */
    public int getDeuxPointsReussis() {
        return deuxPointsReussis;
    }

    /**
     * Méthode permettant d'accéder au nombre de trois points tentés
     * @return le nombre trois points réussi
     */
    public int getTroisPoints() {
        return troisPoints;
    }

    /**
     * Méthode permettant d'accéder au nombre de trois points reussis
     * @return le nombre de trois points reussis
     */
    public int getTroisPointsReussis() {
        return troisPointsReussis;
    }

    /**
     * Méthode permettant d'accéder au nombre de rebonds
     * @return le nombre de rebonds
     */
    public int getRebonds() {
        return rebonds;
    }

    /**
     * Méthode permettant d'accéder au nombre de fautes
     * @return le nombre de fautes
     */
    public int getFautes() {
        return fautes;
    }

    /**
     * Méthode permettant d'accéder au nombre de passes decisives
     * @return le nombre de passes decisives
     */
    public int getPassesDecisives() {
        return passesDecisives;
    }

    /**
     * Méthode permettant d'accéder au nombre de ballons perdus
     * @return le nombre de ballons perdus
     */
    public int getBallonsPerdus() {
        return ballonsPerdus;
    }

    /**
     * Méthode permettant d'accéder au nombre d'interceptions
     * @return le nombre d'interceptions
     */
    public int getInterceptions() {
        return interceptions;
    }

    /**
     * Méthode permettant d'accéder au nombre de contres
     * @return le nombre de contres
     */
    public int getContres() {
        return contres;
    }
}
