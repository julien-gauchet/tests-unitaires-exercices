package fr.julien.formation.basket.lancement;

import java.sql.Connection;
import java.sql.SQLException;

import fr.julien.formation.basket.exception.BasketException;
import fr.julien.formation.basket.model.Equipe;
import fr.julien.formation.basket.services.matchs.MiseAJourMatchs;
import fr.julien.formation.basket.services.utils.ConnexionFactory;

/**
 * Classe permettant de lancer l'import des résultats de l'équipe d'orléans
 * @author Julien Gauchet
 */
public final class LanceurImportResultats {

    private LanceurImportResultats() {
        super();
    }

    /**
     * Méthode de lancement du programme
     * @param args
     *            Tableau contenant [le nom du driver de connexion, l'url de connexion, le user, le mot de passe, la journée concernée par la mise à jour]
     */
    public static void main(String[] args) {
        String driverClassName = args[0];
        String url = args[1];
        String username = args[2];
        String password = args[3];
        int journee = Integer.parseInt(args[4]);
        try (Connection cnx = ConnexionFactory.getInstance().generer(driverClassName, url, username, password)) {
            MiseAJourMatchs.getInstance().mettreAJour(cnx, new Equipe("Orléans"), journee);
        }
        catch (SQLException | BasketException e) {
            e.printStackTrace();
        }
    }
}
