package fr.julien.graphiques.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.h2.jdbcx.JdbcDataSource;
import org.h2.tools.RunScript;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

@ActiveProfiles("test")
@ExtendWith({ SpringExtension.class })
@ContextConfiguration(classes = { ConfigTest.class })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class })
public abstract class AbstractTestBdd {
    @Autowired
    private DataSource dataSource;
    protected Connection connection;

    public void initConnection() {
	try {
	    connection = dataSource.getConnection();
	    JdbcDataSource dataSource = new JdbcDataSource();
	    dataSource.setURL("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1");
	    dataSource.setUser("sa");
	    dataSource.setPassword("");
	    Connection cnx = dataSource.getConnection();
	    try (InputStreamReader fr = new FileReader(new File("src/test/resources/init_db2.sql")); BufferedReader br = new BufferedReader(fr)) {
		RunScript.execute(cnx, br);
	    }
	}
	catch (Exception e) {
	    e.printStackTrace();
	}
    }

    public void closeConnection() {
	try {
	    connection.close();
	}
	catch (SQLException e) {
	    e.printStackTrace();
	}
    }
}
