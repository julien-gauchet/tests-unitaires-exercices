package fr.julien.graphiques.services;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

import fr.julien.graphiques.config.AbstractTestBdd;
import fr.julien.graphiques.exception.StatistiqueException;
import fr.julien.graphiques.modele.dto.ResultatRegression;

@ActiveProfiles("test")
public class GenerationRegressionServiceTest extends AbstractTestBdd {
    @BeforeEach
    public void init() {
	initConnection();
    }

    @AfterEach
    public void end() {
	closeConnection();
    }

    @Autowired
    private GenerationRegressionService generationRegressionService;

    @Test
    public void testerGenerationRegression() {
	try {
	    ResultatRegression res = generationRegressionService.genererRegression();
	    Assertions.assertEquals(2.87d, res.getB0(), 0.1d);
	}
	catch (StatistiqueException e) {
	    e.printStackTrace();
	    Assertions.fail();
	}
    }
}
