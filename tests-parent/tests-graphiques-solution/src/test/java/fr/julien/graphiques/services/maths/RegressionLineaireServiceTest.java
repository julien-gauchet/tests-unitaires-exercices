package fr.julien.graphiques.services.maths;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

import fr.julien.graphiques.config.AbstractTest;
import fr.julien.graphiques.modele.dto.NuagePoints;
import fr.julien.graphiques.modele.dto.Point;
import fr.julien.graphiques.modele.dto.ResultatRegression;

@ActiveProfiles("test")
public class RegressionLineaireServiceTest extends AbstractTest {

    @Autowired
    private RegressionLineaireService regressionLineaireService;

    @Test
    public void testerMoyenne() {
        try {
            NuagePoints nuage = new NuagePoints(null, null);
            nuage.add(new Point(3, 3));
            nuage.add(new Point(4, 5));
            nuage.add(new Point(5, 5));
            nuage.add(new Point(6, 6));
            nuage.add(new Point(7, 5));
            nuage.add(new Point(8, 8));
            ResultatRegression res= regressionLineaireService.calculer(nuage);
            Assertions.assertEquals(1.24d, res.getB0(), 0.1d);
            Assertions.assertEquals(0.7d, res.getB1(), 0.1d);
        }
        catch (Exception e) {
            e.printStackTrace();
            Assertions.fail();
        }
    }
}
