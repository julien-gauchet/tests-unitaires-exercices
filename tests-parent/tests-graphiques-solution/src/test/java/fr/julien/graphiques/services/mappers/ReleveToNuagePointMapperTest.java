package fr.julien.graphiques.services.mappers;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

import fr.julien.graphiques.config.AbstractTest;
import fr.julien.graphiques.modele.dto.NuagePoints;
import fr.julien.graphiques.modele.entites.Concentration;
import fr.julien.graphiques.modele.entites.Minerai;
import fr.julien.graphiques.modele.entites.Releve;

@ActiveProfiles("test")
public class ReleveToNuagePointMapperTest extends AbstractTest {

    @Autowired
    private ReleveToNuagePointMapper releveToNuagePointMapper;

    @Test
    public void testerMapping() {
        List<Concentration> concentrations = new ArrayList<>();
        Releve r = new Releve();
        Minerai ni = new Minerai();
        ni.setId("ni");
        Minerai co = new Minerai();
        co.setId("co");
        r.setId(1);
        Concentration c1 = new Concentration();
        c1.setReleve(r);
        c1.setMinerai(ni);
        c1.setValeur(2d);
        Concentration c2 = new Concentration();
        c2.setReleve(r);
        c2.setMinerai(ni);
        c2.setValeur(3d);
        concentrations.add(c1);
        concentrations.add(c2);
        NuagePoints n = releveToNuagePointMapper.releveToNuagePoint(concentrations);
        Assertions.assertEquals(1, n.getX().size());
    }
}
