package fr.julien.graphiques.dao.repository;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

import fr.julien.graphiques.config.AbstractTestBdd;

@ActiveProfiles("test")
public class ReleveRepositoryTest extends AbstractTestBdd {
    @BeforeEach
    public void init() {
	initConnection();
    }

    @AfterEach
    public void end() {
	closeConnection();
    }

    @Autowired
    private ConcentrationsRepository relevesRepository;

    @Test
    public void testerReleveRepository() {
	try {
	    relevesRepository.findStatistiques();
	}
	catch (Exception e) {
	    e.printStackTrace();
	    Assertions.fail();
	}
    }
}
