package fr.julien.graphiques.services.maths;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

import fr.julien.graphiques.config.AbstractTest;
import fr.julien.graphiques.exception.StatistiqueException;
import fr.julien.graphiques.modele.dto.Vecteur;

@ActiveProfiles("test")
public class VecteursServiceTest extends AbstractTest {

    @Autowired
    private VecteursService vecteursService;

    @Test
    public void testerTransposition() {
        Vecteur v = new Vecteur(2, 3);
        v.add(2d);
        v.add(3d);
        Vecteur res = vecteursService.transposer(v);
        Assertions.assertNotEquals(v.isColonne(), res.isColonne());
    }

    @Test
    public void testerAjout() {
        try {
            Vecteur res = vecteursService.ajouter(new Vecteur(2, 3), new Vecteur(1, 1));
            Assertions.assertEquals(Double.valueOf(3), res.get(0));
            Assertions.assertEquals(Double.valueOf(4), res.get(1));
        }
        catch (StatistiqueException e) {
            e.printStackTrace();
            Assertions.fail();
        }
    }

    @Test
    public void testerMultiplier() {
        try {
            Vecteur res = vecteursService.multiplier(new Vecteur(2, 3), 2);
            Assertions.assertEquals(Double.valueOf(4), res.get(0));
            Assertions.assertEquals(Double.valueOf(6), res.get(1));
        }
        catch (Exception e) {
            e.printStackTrace();
            Assertions.fail();
        }
    }

    @Test
    public void testerMin() {
        try {
            Assertions.assertEquals(2d, vecteursService.min(new Vecteur(2, 3)), 0.1d);
        }
        catch (Exception e) {
            e.printStackTrace();
            Assertions.fail();
        }
    }

    @Test
    public void testerMax() {
        try {
            Assertions.assertEquals(3d, vecteursService.max(new Vecteur(2, 3)), 0.1d);
        }
        catch (Exception e) {
            e.printStackTrace();
            Assertions.fail();
        }
    }

    @Test
    public void testerProduit() {
        try {
            Vecteur v2 = new Vecteur(2, 2);
            Vecteur v = new Vecteur(2, 3);
            v.setColonne(!v.isColonne());
            Double res = vecteursService.produit(v, v2);
            Assertions.assertEquals(Double.valueOf(10), res);
        }
        catch (StatistiqueException e) {
            e.printStackTrace();
            Assertions.fail();
        }
    }
}
