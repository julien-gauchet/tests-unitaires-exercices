package fr.julien.graphiques.services.maths;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

import fr.julien.graphiques.config.AbstractTest;
import fr.julien.graphiques.exception.StatistiqueException;
import fr.julien.graphiques.modele.dto.Vecteur;

@ActiveProfiles("test")
public class StatistiquesServiceTest extends AbstractTest {

    @Autowired
    private StatistiquesService statistiquesService;

    @Test
    public void testerMoyenne() {
	Assertions.assertEquals(5.7d, statistiquesService.moyenne(new Vecteur(3, 4, 5, 6, 7, 9)), 0.1d);
    }

    @Test
    public void testerSomme() {
	Assertions.assertEquals(34d, statistiquesService.somme(new Vecteur(3, 4, 5, 6, 7, 9)), 0.1d);
    }

    @Test
    public void testerVariance() {
	Assertions.assertEquals(3.89d, statistiquesService.variance(new Vecteur(3, 4, 5, 6, 7, 9)), 0.1d);
    }

    @Test
    public void testerCovariance() {
        try {
            Assertions.assertEquals(4.5d, statistiquesService.covariance(new Vecteur(3, 4, 5, 6, 7, 9), new Vecteur(2, 4, 5, 9, 7, 9)), 0.1d);
        }
        catch (StatistiqueException e) {
            e.printStackTrace();
            Assertions.fail();
        }
    }
    
    @Test
    public void testerCorrelation() {
        try {
            Assertions.assertEquals(0.17d, statistiquesService.correlation(new Vecteur(3, 4, 5, 6, 7, 9), new Vecteur(2, 4, 5, 9, 7, 9)), 0.1d);
        }
        catch (StatistiqueException e) {
            e.printStackTrace();
            Assertions.fail();
        }
    }
}
