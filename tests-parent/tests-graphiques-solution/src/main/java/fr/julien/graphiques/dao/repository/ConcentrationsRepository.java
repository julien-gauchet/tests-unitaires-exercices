package fr.julien.graphiques.dao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.julien.graphiques.modele.entites.Concentration;

public interface ConcentrationsRepository extends JpaRepository<Concentration, Integer> {

    @Query("SELECT c FROM Concentration c WHERE c.minerai.id IN ('ni', 'co')")
    public List<Concentration> findStatistiques();
}
