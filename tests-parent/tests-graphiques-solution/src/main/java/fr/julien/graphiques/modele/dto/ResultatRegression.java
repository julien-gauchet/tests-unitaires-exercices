package fr.julien.graphiques.modele.dto;

public class ResultatRegression {

    /** coefficient constant */
    private double b0;
    /** pente de la droite */
    private double b1;
    /** coéfficient de détermination */
    private double r2;
    /** somme des carrés résiduels */
    private double sommeCarresResiduels;
    /** somme des carrés expliqués */
    private double sommeCarresExpliques;
    /** somme des carrés totaux */
    private double sommeCarres;
    /** estimateur de la variance des résidus */
    private double sigma;
    /** coéfficient de détermination ajusté */
    private double coefficientDeterminationAjuste;
    private NuagePoints nuage;

    public double getB0() {
        return b0;
    }

    public void setB0(double b0) {
        this.b0 = b0;
    }

    public double getB1() {
        return b1;
    }

    public void setB1(double b1) {
        this.b1 = b1;
    }

    public double getR2() {
        return r2;
    }

    public void setR2(double r2) {
        this.r2 = r2;
    }

    public double getSommeCarresResiduels() {
        return sommeCarresResiduels;
    }

    public void setSommeCarresResiduels(double sommeCarresResiduels) {
        this.sommeCarresResiduels = sommeCarresResiduels;
    }

    public double getSommeCarresExpliques() {
        return sommeCarresExpliques;
    }

    public void setSommeCarresExpliques(double sommeCarresExpliques) {
        this.sommeCarresExpliques = sommeCarresExpliques;
    }

    public double getSommeCarres() {
        return sommeCarres;
    }

    public void setSommeCarres(double sommeCarres) {
        this.sommeCarres = sommeCarres;
    }

    public double getSigma() {
        return sigma;
    }

    public void setSigma(double sigma) {
        this.sigma = sigma;
    }

    public double getCoefficientDeterminationAjuste() {
        return coefficientDeterminationAjuste;
    }

    public void setCoefficientDeterminationAjuste(double coefficientDeterminationAjuste) {
        this.coefficientDeterminationAjuste = coefficientDeterminationAjuste;
    }

    public NuagePoints getNuage() {
        return nuage;
    }

    public void setNuage(NuagePoints nuage) {
        this.nuage = nuage;
    }
}
