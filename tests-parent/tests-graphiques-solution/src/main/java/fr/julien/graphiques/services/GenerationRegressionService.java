package fr.julien.graphiques.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.julien.graphiques.dao.repository.ConcentrationsRepository;
import fr.julien.graphiques.exception.StatistiqueException;
import fr.julien.graphiques.modele.dto.NuagePoints;
import fr.julien.graphiques.modele.dto.ResultatRegression;
import fr.julien.graphiques.modele.entites.Concentration;
import fr.julien.graphiques.services.mappers.ReleveToNuagePointMapper;
import fr.julien.graphiques.services.maths.RegressionLineaireService;

@Component
public class GenerationRegressionService {

    @Autowired
    private ConcentrationsRepository concentrationsRepository;
    @Autowired
    private ReleveToNuagePointMapper releveToNuagePointMapper;
    @Autowired
    private RegressionLineaireService regressionLineaireService;

    public ResultatRegression genererRegression() throws StatistiqueException {
        List<Concentration> c = concentrationsRepository.findStatistiques();
        NuagePoints nuage = releveToNuagePointMapper.releveToNuagePoint(c);
        ResultatRegression res = regressionLineaireService.calculer(nuage);
        res.setNuage(nuage);
        return res;
    }
}
