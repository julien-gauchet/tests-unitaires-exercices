package fr.julien.graphiques.lancement;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"fr.julien.graphiques"})
public class ConfigGraphique {
}
