package fr.julien.graphiques.modele.dto;

import java.util.ArrayList;

/**
 * 
 * Classe Vecteur
 * 
 * Classe qui permet de définir un vecteur
 * 
 * @author julien
 *
 */
public class Vecteur extends ArrayList<Double> {

    private static final long serialVersionUID = 1L;
    private boolean colonne;

    public Vecteur() {
        super();
        setColonne(true);
    }

    public Vecteur(Double... values) {
        super();
        setColonne(true);
        for (Double d : values) {
            add(d);
        }
    }

    public Vecteur(Integer... values) {
        super();
        setColonne(true);
        for (Integer d : values) {
            add(Double.valueOf(d));
        }
    }

    public boolean isColonne() {
        return colonne;
    }

    public void setColonne(boolean colonne) {
        this.colonne = colonne;
    }
}
