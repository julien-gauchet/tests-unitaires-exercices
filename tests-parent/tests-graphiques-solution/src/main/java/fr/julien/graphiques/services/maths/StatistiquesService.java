package fr.julien.graphiques.services.maths;

import org.springframework.stereotype.Component;

import fr.julien.graphiques.exception.StatistiqueException;
import fr.julien.graphiques.modele.dto.Vecteur;

@Component
public class StatistiquesService {

    public double variance(Vecteur v) {
        double res = 0;
        if (v != null && !v.isEmpty()) {
            double moyenne = moyenne(v);
            for (Double d : v) {
                res += (d - moyenne) * (d - moyenne);
            }
            res = res / v.size();
        }
        return res;
    }

    public double moyenne(Vecteur v) {
        return somme(v) / v.size();
    }

    public double somme(Vecteur v) {
        double res = 0;
        for (Double d : v) {
            res += d;
        }
        return res;
    }

    public double correlation(Vecteur v, Vecteur v2) throws StatistiqueException {
        double res = 0;
        double denom = variance(v) * variance(v2);
        if (denom > 0) {
            res = covariance(v, v2) / denom;
        }
        return res;
    }

    public double covariance(Vecteur v, Vecteur v2) throws StatistiqueException {
        double res = 0;
        if ((v != null && v2 != null && v.size() != v2.size()) || (v == null && v2 != null && !v2.isEmpty()) || (v2 == null && v != null && !v.isEmpty())) {
            throw new StatistiqueException("Les deux vecteurs ont des tailles différentes");
        }
        if (v != null && !v.isEmpty()) {
            double moyenne1 = moyenne(v);
            double moyenne2 = moyenne(v2);
            for (int i = 0; i < v.size(); i++) {
                res += (v.get(i) - moyenne1) * (v2.get(i) - moyenne2);
            }
            res = res / v.size();
        }
        return res;
    }
}
