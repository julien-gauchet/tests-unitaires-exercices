package fr.julien.graphiques.modele.entites;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "concentrations")
public class Concentration {

    @Id
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "id_releve")
    private Releve releve;
    @ManyToOne
    @JoinColumn(name = "id_minerai")
    private Minerai minerai;
    @Column(name = "valeur")
    private Double valeur;

    public Releve getReleve() {
        return releve;
    }

    public void setReleve(Releve releve) {
        this.releve = releve;
    }

    public Minerai getMinerai() {
        return minerai;
    }

    public void setMinerai(Minerai minerai) {
        this.minerai = minerai;
    }

    public Double getValeur() {
        return valeur;
    }

    public void setValeur(Double valeur) {
        this.valeur = valeur;
    }
}
