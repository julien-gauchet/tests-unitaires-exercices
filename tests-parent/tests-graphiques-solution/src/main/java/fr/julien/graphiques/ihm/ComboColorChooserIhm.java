package fr.julien.graphiques.ihm;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import fr.julien.graphiques.ihm.ComboColorChooserIhm.MyColor;

/**
 * 
 * Classe ComboColorChooser
 * 
 * Classe qui définit un comboBox avec une liste de couleurs
 * 
 * @author julien
 *
 */
public class ComboColorChooserIhm extends JComboBox<MyColor> {

    private static final long serialVersionUID = 1L;
    /** colors : les couleurs affichées */
    private MyColor[] colors;

    public ComboColorChooserIhm() {
        initializeCombo();
    }

    private void initializeCombo() {
        colors = new MyColor[11];
        colors[0] = new MyColor(Color.BLACK.getRGB(), "Noir");
        colors[1] = new MyColor(Color.GRAY.getRGB(), "Gris");
        colors[2] = new MyColor(Color.BLUE.getRGB(), "Bleu");
        colors[3] = new MyColor(Color.CYAN.getRGB(), "Cyan");
        colors[4] = new MyColor(Color.RED.getRGB(), "Rouge");
        colors[5] = new MyColor(Color.MAGENTA.getRGB(), "Magenta");
        colors[6] = new MyColor(Color.PINK.getRGB(), "Rose");
        colors[7] = new MyColor(Color.ORANGE.getRGB(), "Orange");
        colors[8] = new MyColor(Color.GREEN.getRGB(), "Vert");
        colors[9] = new MyColor(Color.YELLOW.getRGB(), "Jaune");
        colors[10] = new MyColor(Color.WHITE.getRGB(), "Blanc");
        DefaultComboBoxModel<MyColor> model = new DefaultComboBoxModel<MyColor>(colors);
        setModel(model);
        setRenderer(new ListCellRenderer<Object>() {

            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                MyColor myColor = (MyColor) value;
                int w = 20;
                int h = 20;
                BufferedImage img = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
                Graphics g = img.getGraphics();
                g.setColor(myColor);
                g.fillRect(0, 0, w, h);
                ImageIcon icon = new ImageIcon(img);
                JLabel label;
                label = new JLabel(myColor.getColorName(), icon, JLabel.LEADING);
                g.setColor(Color.white);
                g.drawRect(0, 0, w, h);
                return label;
            }
        });
    }

    class MyColor extends Color {

        private static final long serialVersionUID = 1L;
        private String colorName;

        public MyColor(int rgb, String colorName) {
            super(rgb);
            this.colorName = colorName;
        }

        public MyColor(int rgba, boolean hasalpha, String colorName) {
            super(rgba, hasalpha);
            this.colorName = colorName;
        }

        public MyColor(int r, int g, int b, String colorName) {
            super(r, g, b);
            this.colorName = colorName;
        }

        public MyColor(float r, float g, float b, String colorName) {
            super(r, g, b);
            this.colorName = colorName;
        }

        public MyColor(ColorSpace cspace, float[] components, float alpha, String colorName) {
            super(cspace, components, alpha);
            this.colorName = colorName;
        }

        public MyColor(int r, int g, int b, int a, String colorName) {
            super(r, g, b, a);
            this.colorName = colorName;
        }

        public MyColor(float r, float g, float b, float a, String colorName) {
            super(r, g, b, a);
            this.colorName = colorName;
        }

        public String getColorName() {
            return colorName;
        }
    }
}
