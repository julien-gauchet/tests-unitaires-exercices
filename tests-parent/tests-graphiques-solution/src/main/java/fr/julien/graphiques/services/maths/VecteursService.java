package fr.julien.graphiques.services.maths;

import org.springframework.stereotype.Component;

import fr.julien.graphiques.exception.StatistiqueException;
import fr.julien.graphiques.modele.dto.Vecteur;

@Component
public class VecteursService {

    public Vecteur transposer(Vecteur v) {
        Vecteur t = new Vecteur();
        t.addAll(v);
        t.setColonne(!v.isColonne());
        return t;
    }

    public Vecteur ajouter(Vecteur v, Vecteur v2) throws StatistiqueException {
        Vecteur s = new Vecteur();
        if (v.size() != v2.size()) {
            throw new StatistiqueException("Vecteurs incompatibles pour une somme");
        }
        for (int k = 0; k < v.size(); k++) {
            s.add(v2.get(k) + v.get(k));
        }
        return s;
    }

    public double produit(Vecteur v, Vecteur v2) throws StatistiqueException {
        double p = 0;
        if (v.size() != v2.size() || v.isColonne() || !v2.isColonne()) {
            throw new StatistiqueException("Vecteurs incompatibles pour un produit");
        }
        for (int k = 0; k < v.size(); k++) {
            p += v.get(k) * v2.get(k);
        }
        return p;
    }

    public double max(Vecteur v) {
        double max = v.get(0);
        for (int i = 1; i < v.size(); i++) {
            if (v.get(i) > max) {
                max = v.get(i);
            }
        }
        return max;
    }

    public double min(Vecteur v) {
        double min = v.get(0);
        for (int i = 1; i < v.size(); i++) {
            if (v.get(i) < min) {
                min = v.get(i);
            }
        }
        return min;
    }

    public Vecteur multiplier(Vecteur v, double a) {
        Vecteur res = new Vecteur();
        for (int k = 0; k < v.size(); k++) {
            res.add(a * v.get(k));
        }
        return res;
    }
}
