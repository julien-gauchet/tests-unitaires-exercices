package fr.julien.graphiques.services.mappers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import fr.julien.graphiques.modele.dto.NuagePoints;
import fr.julien.graphiques.modele.dto.Point;
import fr.julien.graphiques.modele.entites.Concentration;

@Component
public class ReleveToNuagePointMapper {

    public NuagePoints releveToNuagePoint(List<Concentration> concentrations) {
        NuagePoints nuage = new NuagePoints(null, null);
        Map<Integer, Point> points = new HashMap<>();
        for (Concentration c : concentrations) {
            Point p = new Point();
            if (points.containsKey(c.getReleve().getId())) {
                p = points.get(c.getReleve().getId());
            }
            else {
                points.put(c.getReleve().getId(), p);
            }
            if (c.getMinerai().getId().equals("ni")) {
                p.setX(c.getValeur());
            }
            else {
                p.setY(c.getValeur());
            }
            points.remove(c.getReleve().getId());
            points.put(c.getReleve().getId(), p);
        }
        for (Point p : points.values()) {
            nuage.add(p);
        }
        return nuage;
    }
}
